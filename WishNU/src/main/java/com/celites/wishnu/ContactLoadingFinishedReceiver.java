package com.celites.wishnu;

import com.celites.wishnu.utils.Constants;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ContactLoadingFinishedReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Intent activityIntent = new Intent(context, BaseActivity.class);
		activityIntent.putExtra(Constants.EXTRA_FRAGMENT_INDEX,
				intent.getIntExtra(Constants.EXTRA_FRAGMENT_INDEX, -1));
		activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(activityIntent);
	}

}
