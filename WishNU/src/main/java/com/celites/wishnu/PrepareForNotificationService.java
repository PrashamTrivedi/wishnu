package com.celites.wishnu;

import android.app.IntentService;
import android.content.Intent;
import com.celites.wishnu.data.FriendsData;
import com.celites.wishnu.datautils.DBUtils;
import com.celites.wishnu.datautils.DatabaseHelper;
import com.celites.wishnu.utils.ConstantMethods;
import java.util.ArrayList;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class PrepareForNotificationService
		extends IntentService {

	public PrepareForNotificationService() {
		super("PrepareForNotificationService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		DatabaseHelper dbHelper = new DatabaseHelper(this);
		dbHelper.open();
		ArrayList<FriendsData> contactsForBirthday = dbHelper.getFreindsForBirthdayNotification(DBUtils.CONTACTS_TABLE_NAME);
		contactsForBirthday.addAll(dbHelper.getFreindsForBirthdayNotification(DBUtils.GOOGLE_TABLE_NAME));

		ArrayList<FriendsData> contactsForAnniversary = dbHelper.getFreindsForAnniversaryNotification(DBUtils.CONTACTS_TABLE_NAME);
		contactsForAnniversary.addAll(dbHelper.getFreindsForAnniversaryNotification(DBUtils.GOOGLE_TABLE_NAME));


		for (FriendsData friend: contactsForAnniversary) {
			ConstantMethods.setAlarm(this,friend,false);
		}
		for (FriendsData friend : contactsForBirthday) {
			ConstantMethods.setAlarm(this, friend, true);
		}
	}


}
