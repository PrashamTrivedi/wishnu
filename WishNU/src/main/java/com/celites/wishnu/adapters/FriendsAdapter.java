package com.celites.wishnu.adapters;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.celites.wishnu.BaseActivity;
import com.celites.wishnu.R;
import com.celites.wishnu.customViews.CharacterDrawable;
import com.celites.wishnu.data.ContactType;
import com.celites.wishnu.data.FriendsData;
import com.celites.wishnu.fragments.FriendsDetailFragment;
import com.celites.wishnu.utils.AnalyticsLogger;
import com.celites.wishnu.utils.ConstantMethods;
import java.util.ArrayList;

/**
 * Created by Prasham on 18-12-2014.
 */
public class FriendsAdapter
		extends Adapter<FriendsAdapter.ViewHolder>
		implements Filterable {
	private final BaseActivity baseActivity;
	private final ArrayList<FriendsData> friends;
	private ArrayList<FriendsData> filteredCollection;
	private final int color;
	private ContactType type;

	/**
	 * <p>Returns a filter that can be used to constrain data with a filtering pattern.</p> <p/> <p>This method is usually implemented by {@link
	 * android.widget.Adapter} classes.</p>
	 *
	 * @return a filter used to constrain data
	 */
	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				ArrayList<FriendsData> filteredFriends = new ArrayList<FriendsData>();
				FilterResults filterFriends = new FilterResults();
				if (!ConstantMethods.isEmptyString(constraint)) {
					for (FriendsData friend : friends) {
						String name = friend.getName();
						try {
							if (name.toLowerCase().contains(constraint.toString().toLowerCase())) {
								filteredFriends.add(friend);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				} else {
					filteredFriends = friends;
				}
				filterFriends.count = filteredFriends.size();
				filterFriends.values = filteredFriends;
				return filterFriends;
			}

			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				filteredCollection = (ArrayList<FriendsData>) results.values;
				AnalyticsLogger init = AnalyticsLogger.init(baseActivity);
				if (!ConstantMethods.isArrayListEmpty(filteredCollection)) {
					init.sendEvent("MenuItem", "Search", "Results " + filteredCollection.size());
				} else {
					init.sendEvent("MenuItem", "Search", "Results 0");
				}

				notifyDataSetChanged();
			}
		};
		return filter;
	}

	/**
	 * Called when RecyclerView needs a new {@link com.celites.wishnu.adapters.FriendsAdapter.ViewHolder} of the given type to represent an item.
	 * <p/>
	 * This new ViewHolder should be constructed with a new View that can represent the items of the given type. You can either create a new View
	 * manually or inflate it from an XML layout file.
	 * <p/>
	 * The new ViewHolder will be used to display items of the adapter using {@link #onBindViewHolder(com.celites.wishnu.adapters.FriendsAdapter.ViewHolder,
	 * int)}. Since it will be re-used to display different items in the data set, it is a good idea to cache references to sub views of the View to
	 * avoid unnecessary {@link View#findViewById(int)} calls.
	 *
	 * @param parent
	 * 		The ViewGroup into which the new View will be added after it is bound to an adapter position.
	 * @param viewType
	 * 		The view type of the new View.
	 *
	 * @return A new ViewHolder that holds a View of the given view type.
	 *
	 * @see #getItemViewType(int)
	 * @see #onBindViewHolder(com.celites.wishnu.adapters.FriendsAdapter.ViewHolder, int)
	 */
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(baseActivity);
		View view = inflater.inflate(R.layout.friendslistitem, parent, false);
		return new ViewHolder(view);
	}

	/**
	 * Called by RecyclerView to display the data at the specified position. This method should update the contents of the {@link
	 * com.celites.wishnu.adapters.FriendsAdapter.ViewHolder#itemView} to reflect the item at the given position.
	 * <p/>
	 * Note that unlike {@link android.widget.ListView}, RecyclerView will not call this method again if the position of the item changes in the data
	 * set unless the item itself is invalidated or the new position cannot be determined. For this reason, you should only use the
	 * <code>position</code> parameter while acquiring the related data item inside this method and should not keep a copy of it. If you need the
	 * position of an item later on (e.g. in a click listener), use {@link com.celites.wishnu.adapters.FriendsAdapter.ViewHolder#getPosition()} which
	 * will have the updated position.
	 *
	 * @param holder
	 * 		The ViewHolder which should be updated to represent the contents of the item at the given position in the data set.
	 * @param position
	 * 		The position of the item within the adapter's data set.
	 */
	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		FriendsData friend = getItem(position);
		holder.friend = friend;
		String uri = friend.getImageUri();
		if (ConstantMethods.isEmptyString(uri)) {
			CharacterDrawable drawable = new CharacterDrawable(friend.getInitials(), color);

			holder.image.setImageDrawable(drawable);

		} else {
			ConstantMethods.getPicasso(baseActivity).load(uri).into(holder.image);
		}
		holder.name.setTextColor(color);
		holder.name.setText(friend.getName());
		String birthdateString = baseActivity.getString(R.string.birthdayHeader, ConstantMethods.getDateStringToShow(friend.getBirthDate()));
		String birthdateText = (friend.getBirthDate() <= 0) ? "Birthdate not available"
		                                                    : (friend.isShouldNotify()) ? "<b>" + birthdateString + "</b>" : birthdateString;
		String anniversaryString = baseActivity.getString(R.string.anniversaryHeader, ConstantMethods.getDateStringToShow(friend.getAnniversary()));
		String anniversaryText =
				(friend.getAnniversary() <= 0) ? "" : (friend.isShouldNotifyAnniversary()) ? "<b>" + anniversaryString + "</b>" : anniversaryString;
		holder.birthdate.setText(Html.fromHtml(birthdateText));
		holder.anniversary.setText(Html.fromHtml(anniversaryText));
	}

	public FriendsData getItem(int position) {
		return filteredCollection.get(position);
	}

	/**
	 * Returns the total number of items in the data set hold by the adapter.
	 *
	 * @return The total number of items in this adapter.
	 */
	@Override
	public int getItemCount() {
		return filteredCollection.size();
	}

	public FriendsAdapter(BaseActivity baseActivity, ArrayList<FriendsData> friends, int color, ContactType type) {
		this.baseActivity = baseActivity;
		this.friends = friends;
		this.filteredCollection = friends;
		this.color = color;

		this.type = type;
	}

	public class ViewHolder
			extends RecyclerView.ViewHolder {
		ImageView image;
		TextView name;
		TextView birthdate;
		TextView anniversary;
		public FriendsData friend;

		public ViewHolder(View itemView) {
			super(itemView);
			birthdate = (TextView) itemView.findViewById(R.id.birthday);
			anniversary = (TextView) itemView.findViewById(R.id.anniversary);
			image = (ImageView) itemView.findViewById(R.id.contactImage);
			name = (TextView) itemView.findViewById(R.id.contactName);
			itemView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {


					if (friend != null) {
						friend.setType(type);


						FriendsDetailFragment friendsDetailFragment = new FriendsDetailFragment();
						Bundle bundle = new Bundle();
						bundle.putParcelable(baseActivity.getString(R.string.friends_extra), friend);
						friendsDetailFragment.setArguments(bundle);
						ConstantMethods.openFragment(baseActivity, friendsDetailFragment, FriendsDetailFragment.TAG);
					}
				}
			});
		}
	}
}
