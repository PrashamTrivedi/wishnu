package com.celites.wishnu.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.celites.wishnu.BaseActivity;
import com.celites.wishnu.R;
import com.celites.wishnu.data.CelebrityModel;
import com.celites.wishnu.fragments.CelebretiesDetailFragment;
import com.celites.wishnu.utils.ConstantMethods;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by Prasham on 16-12-2014.
 */
public class BornTodayAdapter
		extends Adapter<BornTodayAdapter.ViewHolder> {
	private Context context;
	private ArrayList<CelebrityModel> models;
	private BaseActivity baseActivity;

	/**
	 * Called when RecyclerView needs a new {@link com.celites.wishnu.adapters.BornTodayAdapter.ViewHolder} of the given type to represent an item.
	 * <p/>
	 * This new ViewHolder should be constructed with a new View that can represent the items of the given type. You can either create a new View
	 * manually or inflate it from an XML layout file.
	 * <p/>
	 * The new ViewHolder will be used to display items of the adapter using {@link #onBindViewHolder(com.celites.wishnu.adapters.BornTodayAdapter.ViewHolder,
	 * int)}. Since it will be re-used to display different items in the data set, it is a good idea to cache references to sub views of the View to
	 * avoid unnecessary {@link View#findViewById(int)} calls.
	 *
	 * @param parent
	 * 		The ViewGroup into which the new View will be added after it is bound to an adapter position.
	 * @param viewType
	 * 		The view type of the new View.
	 *
	 * @return A new ViewHolder that holds a View of the given view type.
	 *
	 * @see #getItemViewType(int)
	 * @see #onBindViewHolder(com.celites.wishnu.adapters.BornTodayAdapter.ViewHolder, int)
	 */
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View convertView = inflater.inflate(R.layout.celebrety_list_item, parent, false);

		return new ViewHolder(convertView);
	}

	public CelebrityModel getItem(int position) {
		return models.get(position);
	}

	/**
	 * Called by RecyclerView to display the data at the specified position. This method should update the contents of the  to reflect the item at the
	 * given position.
	 * <p/>
	 * Note that unlike {@link android.widget.ListView}, RecyclerView will not call this method again if the position of the item changes in the data
	 * set unless the item itself is invalidated or the new position cannot be determined. For this reason, you should only use the
	 * <code>position</code> parameter while acquiring the related data item inside this method and should not keep a copy of it. If you need the
	 * position of an item later on (e.g. in a click listener), use {@link com.celites.wishnu.adapters.BornTodayAdapter.ViewHolder#getPosition()}
	 * which will have the updated position.
	 *
	 * @param holder
	 * 		The ViewHolder which should be updated to represent the contents of the item at the given position in the data set.
	 * @param position
	 * 		The position of the item within the adapter's data set.
	 */
	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		CelebrityModel celebrity = getItem(position);
		holder.celebrity = celebrity;
		Picasso picasso = ConstantMethods.getPicasso(context);
		picasso.load("https://usercontent.googleapis.com/freebase/v1/image/" + celebrity.getId()).into(holder.image);
		holder.name.setText(celebrity.getName());
		String birthdateText = (celebrity.getBirthDate() == null) ? "Birthdate not available"
		                                                          : context.getString(R.string.birthdayHeader, celebrity.getBirthDate().toString());
		holder.birthdate.setText(birthdateText);
		holder.notableFor.setText(celebrity.getKnownFor());


	}

	/**
	 * Returns the total number of items in the data set hold by the adapter.
	 *
	 * @return The total number of items in this adapter.
	 */
	@Override
	public int getItemCount() {
		return models.size();
	}

	public void setModels(ArrayList<CelebrityModel> models) {
		this.models = models;
	}

	public BornTodayAdapter(BaseActivity baseActivity) {
		this.context = baseActivity;

		this.baseActivity = baseActivity;
		this.models = new ArrayList<CelebrityModel>();
	}

	public class ViewHolder
			extends RecyclerView.ViewHolder {
		ImageButton shareImage;
		ImageView image;
		TextView name;
		TextView birthdate;
		TextView notableFor;
		public CelebrityModel celebrity;

		public ViewHolder(View itemView) {
			super(itemView);
			birthdate = (TextView) itemView.findViewById(R.id.birthday);
			notableFor = (TextView) itemView.findViewById(R.id.anniversary);
			image = (ImageView) itemView.findViewById(R.id.contactImage);
			name = (TextView) itemView.findViewById(R.id.contactName);
			shareImage = (ImageButton) itemView.findViewById(R.id.shareBirthday);
			ConstantMethods.applyTintingToImageView(context.getResources(), shareImage, android.R.color.black);
			itemView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					CelebretiesDetailFragment celebretiesDetailFragment = new CelebretiesDetailFragment();
					Bundle bundle = new Bundle();
					bundle.putParcelable(baseActivity.getString(R.string.friends_extra), celebrity);
					celebretiesDetailFragment.setArguments(bundle);
					ConstantMethods.openFragment(baseActivity, celebretiesDetailFragment, CelebretiesDetailFragment.TAG);

				}
			});
			shareImage.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					final Intent emailIntent;
					String recoBody = context.getString(R.string.spread_us_messege);

					emailIntent = new Intent(android.content.Intent.ACTION_SEND);
					emailIntent.setType("image/*");
					emailIntent.putExtra(Intent.EXTRA_STREAM,
					                     "https://usercontent.googleapis.com/freebase/v1/image/" + celebrity.getId());// + " 5th Jan 2010 ");

					emailIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.birthday_share_message, celebrity.getName()));
					context.startActivity(emailIntent);
				}
			});
		}
	}
}
