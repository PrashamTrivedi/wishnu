package com.celites.wishnu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.celites.wishnu.utils.AnalyticsLogger;
import com.celites.wishnu.utils.ConstantMethods;

public class NotificationActionReceiver
		extends BroadcastReceiver {
	public NotificationActionReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();

		AnalyticsLogger analyticsLogger = AnalyticsLogger.init(context);
		if(!ConstantMethods.isEmptyString(action)) {

			if (action.equalsIgnoreCase("CALL")){
				String data = intent.getStringExtra("data");
				Intent call = new Intent(Intent.ACTION_DIAL);
				analyticsLogger.sendEvent("Phone", "Notification", "Call");
				call.setData(Uri.parse("tel:" + data));
				call.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(call);
			}else if(action.equalsIgnoreCase("SMS")){
				String data = intent.getStringExtra("data");
				Intent sms = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", data, null));
				if (sms.resolveActivity(context.getPackageManager()) != null) {
					analyticsLogger.sendEvent("Phone", "CallButton", "SMS");
					sms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(sms);
				}
			}
		}
	}
}
