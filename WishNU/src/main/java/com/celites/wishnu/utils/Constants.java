/**
 *
 */
package com.celites.wishnu.utils;

import android.graphics.Bitmap;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * @author Prasham
 */
public class Constants {

	public static final String FACEBOOK_BUNDLE_KEY = "com.celite.fbcalendar.FriendsListActivity.bundle";

	public static final SimpleDateFormat BIRTHDATE_FORMAT_LONG_FACEBOOK = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
	public static final SimpleDateFormat BIRTHDATE_FORMAT_SHORT_FACEBOOK = new SimpleDateFormat("MM/dd", Locale.getDefault());


	public static final SimpleDateFormat BIRTHDATE_FORMAT_LONG_CONTACTS = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
	public static final SimpleDateFormat BIRTHDATE_FORMAT_SHORT_CONTACTS = new SimpleDateFormat("--MM-dd", Locale.getDefault());

	public static final SimpleDateFormat FREEBASE_QUERY_DATE_FORMAT = new SimpleDateFormat("-MM-dd", Locale.getDefault());

	public static final SimpleDateFormat FORMAT_SHOW_DATES = new SimpleDateFormat("MMM dd", Locale.getDefault());

	public static final String bundleKeyStart = "com.wishnu.";
	public static final String SHARED_PREFS = "wishNu";

	public static final String FACEBOOK_ACCESS_TOKEN = "access_token";
	public static final String FACEBOOK_ACCESS_EXPIRES = "access_expires";

	public static final int SORT_BY_NAME = 0;
	public static final int SORT_BY_BIRTHDATE = 1;
	public static final int NO_SORTING = -1;

	public static final String MY_AD_UNIT_ID = "a14eb512b2d750c";
	public static final String RESET_ALARMS = "resetAlarms";


	public static Bitmap TEMP_BITMAP = null;
	public static boolean CAN_SHOW_IMAGES = false;
	public static int SORTING_METHOD = -1;
	public static String WISH_TEXT = "";
	public static int ALARM_OPTIONS = 295;

	public static final int SET_ALARM = 24;
	public static final int ADD_TO_CALENDAR = 211;
	public static final int BOTH = 295;

	public static final int REQUEST_CODE_RESOLVE_ERR = 9000;

	public static final String ACTION_CONTACTS_LOADED = "com.celites.wishnu.contactsLoaded";
	public static final String ACTION_GOOGLE_LOADED = "com.celites.wishnu.googleLoaded";
	public static final String ACTION_BORN_TODAY_LOADED = "com.celites.wishnu.bornTodayLoaded";
	public static final String EXTRA_FRAGMENT_INDEX = "fragmentIndex";

	public static final int FETCH_CONTACTS = 110;
	public static final int FETCH_FACEBOOK = 111;
	public static final int FETCH_GOOGLE = 112;
}
