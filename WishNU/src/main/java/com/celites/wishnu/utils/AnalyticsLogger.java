package com.celites.wishnu.utils;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.DisplayMetrics;
import com.celites.wishnu.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders.EventBuilder;
import com.google.android.gms.analytics.HitBuilders.ExceptionBuilder;
import com.google.android.gms.analytics.HitBuilders.TimingBuilder;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Prasham on 12-04-2014.
 */
public class AnalyticsLogger {

	private static AnalyticsLogger currentInstance;
	private static GoogleAnalytics analytics;
	private static Context context;
	private Tracker tracker;

	private AnalyticsLogger(Tracker tracker) {

		this.tracker = tracker;
	}

	public static AnalyticsLogger init(Context context) {
		AnalyticsLogger.context = context;
		analytics = GoogleAnalytics.getInstance(context);
		Tracker tracker = analytics.newTracker(R.xml.analytics);
		if (currentInstance == null) {

			currentInstance = new AnalyticsLogger(tracker);
		}
//		currentInstance.askUserConsent(context);
		return currentInstance.setTracker(tracker);
	}

	AnalyticsLogger setTracker(Tracker tracker) {
		this.tracker = tracker;
		return this;
	}


	public void askUserConsent(final Context context) {
		final SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_WORLD_WRITEABLE);
		AlertDialog.Builder builder = new Builder(context);
		builder.setTitle("Sending anonymus data").setMessage("Help us to improve by sending usage statistics to us?");
		builder.setPositiveButton(android.R.string.ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				prefs.edit().putBoolean("userConsent", true).apply();
				analytics.setAppOptOut(true);
			}
		}).setNeutralButton("I want to know more", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent view = new Intent();
				view.setAction(Intent.ACTION_VIEW);
				view.setData(Uri.parse("http://prashamtrivedi.github.io/wishnu_analytics_explanation"));
				context.startActivity(view);
			}
		}).setNegativeButton(android.R.string.cancel, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				prefs.edit().putBoolean("userConsent", true).apply();
				analytics.setAppOptOut(true);
			}
		});

		boolean userConsent = !prefs.getBoolean("userConsent", false);
		if (userConsent) {
			Dialog dialog = builder.create();
			if(!dialog.isShowing()) {
				dialog.show();
			}
		}
	}

	public void sendException(String errorMessage, boolean isFatal) {
		tracker.send(new ExceptionBuilder().setDescription(errorMessage).setFatal(isFatal).build());
	}

	public void sendScreenData() {
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		sendEvent("Screen", "Load", "Display Metrics Data " + getDensityData(metrics));
	}

	public void sendScreen(String screenName) {
		tracker.setScreenName(screenName);
	}

	public void sendEvent(String category, String action, String label) {
		tracker.send(new EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
	}

	public String getDensityData(DisplayMetrics metrics) {
		float density = metrics.density;
		int dpi = metrics.densityDpi;
		String humanReadableValue;
		switch (dpi) {

			case DisplayMetrics.DENSITY_HIGH:
				humanReadableValue = "HDPI";
				break;
			case DisplayMetrics.DENSITY_MEDIUM:
				humanReadableValue = "MDPI";
				break;
			case DisplayMetrics.DENSITY_LOW:
				humanReadableValue = "LDPI";
				break;
			case DisplayMetrics.DENSITY_XHIGH:
				humanReadableValue = "XHDPI";
				break;
			case DisplayMetrics.DENSITY_TV:
				humanReadableValue = "TVDPI";
				break;
			case DisplayMetrics.DENSITY_XXHIGH:
				humanReadableValue = "XXHDPI";
				break;
			case DisplayMetrics.DENSITY_XXXHIGH:
				humanReadableValue = "XXXHDPI";
				break;
			default:
				humanReadableValue = "Default";
				break;
		}

		return "(Display Metrics : density = " + density + " dpi = " + dpi + "{" + humanReadableValue + "})";
	}

	public void sendTime(String category, String label, String variable, long value) {
		tracker.send(new TimingBuilder().setCategory(category).setLabel(label).setVariable(variable).setValue(value).build());
	}
}
