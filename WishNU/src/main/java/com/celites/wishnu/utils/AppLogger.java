
package com.celites.wishnu.utils;

import android.util.Log;

/**
 * Created by Prasham on 5/2/14.
 */
public class AppLogger {
	public static final boolean CAN_LOG = true;
	public static void d(String tag,String message){
		if (CAN_LOG) {
			Log.d(tag, message);
		}
	}

	public static void d(String tag,String message,Throwable throwable){
		if (CAN_LOG) {
			Log.d(tag, message, throwable);
		}
	}

	public static void w(String tag, String message) {
		if (CAN_LOG) {
			Log.w(tag, message);
		}
	}

	public static void w(String tag, String message, Throwable throwable) {
		if (CAN_LOG) {
			Log.w(tag, message, throwable);
		}
	}



	public static void i(String tag, String message) {
		if (CAN_LOG) {
			Log.d(tag, message);
		}
	}

	public static void i(String tag, String message, Throwable throwable) {
		if (CAN_LOG) {
			Log.i(tag, message, throwable);
		}
	}

	public static void v(String tag, String message) {
		if (CAN_LOG) {
			Log.v(tag, message);
		}
	}

	public static void v(String tag, String message, Throwable throwable) {
		if (CAN_LOG) {
			Log.v(tag, message, throwable);
		}
	}

	public static void e(String tag, String message) {
		if (CAN_LOG) {
			Log.e(tag, message);
		}
	}

	public static void e(String tag, String message, Throwable throwable) {
		if (CAN_LOG) {
			Log.e(tag, message, throwable);
		}
	}
}
