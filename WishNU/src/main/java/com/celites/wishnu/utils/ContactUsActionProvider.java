/**
 *
 */
package com.celites.wishnu.utils;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.celites.wishnu.R;
import com.celites.wishnu.interfaces.FacebookListener;

/**
 * @author Prasham
 */
public class ContactUsActionProvider
		extends ActionProvider {

	private Context context;
	private FacebookListener listener;
	private Activity activity;

	public ContactUsActionProvider(Context context) {
		super(context);
		this.context = context;
	}

	/*
		 * (non-Javadoc)
		 *
		 * @see com.actionbarsherlock.view.ActionProvider#onCreateActionView()
		 */
	@Override
	public View onCreateActionView() {

		FrameLayout frame = new FrameLayout(context);

		ImageView imageView = new ImageView(context);

		imageView.setImageResource(R.drawable.ic_action_contactus_dark);
		frame.addView(imageView);

		ImageView downArrow = new ImageView(context);
		downArrow.setImageResource(R.drawable.spinner_ab_default_purpletheme);
		frame.addView(downArrow);

		return null;
	}

	@Override
	public View onCreateActionView(MenuItem forItem) {
		return super.onCreateActionView(forItem);
	}

	@Override
	public boolean onPerformDefaultAction() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean hasSubMenu() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void onPrepareSubMenu(SubMenu subMenu) {
		subMenu.clear();
		subMenu.add(R.string.contact_us_short)
		       .setIcon(R.drawable.ic_action_contactus)
		       .setOnMenuItemClickListener(new OnMenuItemClickListener() {

			       @Override
			       public boolean onMenuItemClick(MenuItem item) {
return false;
			       }
		       });

//		subMenu.add(R.string.connect_fb_page).setIcon(R.drawable.facebook_logo)
//		       .setOnMenuItemClickListener(new OnMenuItemClickListener() {
//
//			       @Override
//			       public boolean onMenuItemClick(MenuItem item) {
//				       return true;
//			       }
//		       });
		super.onPrepareSubMenu(subMenu);
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}


}
