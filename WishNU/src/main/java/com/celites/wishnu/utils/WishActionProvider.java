/**
 *
 */
package com.celites.wishnu.utils;

import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.celites.wishnu.R;
import com.celites.wishnu.interfaces.WishActionListener;


/**
 * @author Prasham
 */
public class WishActionProvider
		extends ActionProvider {

	private Context context;
	private WishActionListener wishActionListener;

	public WishActionProvider(Context context) {
		super(context);
		this.context = context;
	}

	public WishActionListener getWishActionListener() {
		return wishActionListener;
	}

	public void setWishActionListener(WishActionListener wishActionListener) {
		this.wishActionListener = wishActionListener;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.actionbarsherlock.view.ActionProvider#onCreateActionView()
	 */
	@Override
	public View onCreateActionView() {

		FrameLayout frame = new FrameLayout(context);

		ImageView imageView = new ImageView(context);

		imageView.setImageResource(R.drawable.ic_launcher);
		frame.addView(imageView);

		ImageView downArrow = new ImageView(context);
		downArrow.setImageResource(R.drawable.spinner_ab_default_purpletheme);
		frame.addView(downArrow);

		return null;
	}


	@Override
	public boolean hasSubMenu() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void onPrepareSubMenu(SubMenu subMenu) {
		subMenu.clear();

		subMenu.add(R.string.wish_via_google_plus)
		       .setIcon(R.drawable.common_signin_btn_icon_disabled_dark)
		       .setOnMenuItemClickListener(new OnMenuItemClickListener() {

			       @Override
			       public boolean onMenuItemClick(MenuItem item) {
				       if (wishActionListener != null)
					       wishActionListener.wishOnGooglePlus();
				       return false;
			       }
		       });
		super.onPrepareSubMenu(subMenu);
	}
}
