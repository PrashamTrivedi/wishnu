package com.celites.wishnu.utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.ColorRes;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.celites.wishnu.BaseActivity;
import com.celites.wishnu.BirthdayNotificationReceiver;
import com.celites.wishnu.BuildConfig;
import com.celites.wishnu.ClearBornTodayReceiver;
import com.celites.wishnu.PreferencesActivity;
import com.celites.wishnu.R;
import com.celites.wishnu.data.FriendsData;
import com.celites.wishnu.datautils.DBUtils;
import com.celites.wishnu.datautils.DatabaseHelper;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.Builder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ConstantMethods {


	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean connectedOrConnecting = false;
		try {
			connectedOrConnecting = cm.getActiveNetworkInfo().isConnectedOrConnecting();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			connectedOrConnecting = false;
		}
		return connectedOrConnecting;

	}

	/**
	 * This method checks if the string is empty or having null value.
	 *
	 * @param string
	 * 		: Charsequence string to check.
	 *
	 * @return <code>true</code> if string is null, blank or having "null" as value
	 */
	public static boolean isEmptyString(CharSequence string) {
		return (TextUtils.isEmpty(string) || string.toString().equalsIgnoreCase("null"));
	}

	public static void openFragment(BaseActivity baseActivity, Fragment fragment, String TAG) {
		int viewId = R.id.contentFrame;
		int commit = baseActivity.getSupportFragmentManager().beginTransaction().replace(viewId, fragment, TAG).addToBackStack(TAG).commit();
	}

	public static long getDateFromString(String birthdate, boolean isFromFacebook) throws ParseException {
		boolean isWithoutYear = birthdate.length() < 10;

		SimpleDateFormat facebookFormat = isWithoutYear ? Constants.BIRTHDATE_FORMAT_SHORT_FACEBOOK : Constants.BIRTHDATE_FORMAT_LONG_FACEBOOK;
		SimpleDateFormat contactsFormat = isWithoutYear ? Constants.BIRTHDATE_FORMAT_SHORT_CONTACTS : Constants.BIRTHDATE_FORMAT_LONG_CONTACTS;

		SimpleDateFormat format = (isFromFacebook) ? facebookFormat : contactsFormat;

		//		SimpleDateFormat format= Constants.FORMAT_SHOW_DATES;
		Date date = format.parse(birthdate);

		return date.getTime();
	}


	public static String getCurrentDateForFreebase() {
		return Constants.FREEBASE_QUERY_DATE_FORMAT.format(new Date());
	}

	public static long parseDate(String birthdate) throws ParseException {

		SimpleDateFormat format = Constants.FORMAT_SHOW_DATES;
		Date date = format.parse(birthdate);

		return date.getTime();
	}

	public static void setAlarm(Context context, FriendsData friend, boolean isBirthday) {

		long date;
		String event;
		if (isBirthday) {
			boolean shouldNotify = friend.isShouldNotify();
			friend.setShouldNotify(!shouldNotify);
			date = friend.getBirthDate();
			event = "Birthday";
		} else {
			boolean shouldNotify = friend.isShouldNotifyAnniversary();
			friend.setShouldNotifyAnniversary(!shouldNotify);
			date = friend.getAnniversary();
			event = "Anniversary";
		}

		if (date > 0) {
			createAlarmIntent(context, friend, date, event);
		}

		String tableName = DBUtils.CONTACTS_TABLE_NAME;
		if (friend.getType() != null) {
			switch (friend.getType()) {
				case ADDRESS_BOOK:
				case JOINED:
				case FACEBOOK:
					tableName = DBUtils.CONTACTS_TABLE_NAME;
					break;
				case GOOGLE:
					tableName = DBUtils.GOOGLE_TABLE_NAME;
					break;
				default:
					tableName = DBUtils.CONTACTS_TABLE_NAME;
					break;
			}
		}
		new DatabaseHelper(context).insert(tableName, friend);


	}


	public static void setTestingAlarm(Context context, FriendsData friend, long date, String event) {
		Intent alarmIntent = new Intent(context, BirthdayNotificationReceiver.class);

		Calendar cal = Calendar.getInstance();
		Date birthdate = getUpcomingBirthday(new Date(date));

		//		cal.setTime(birthdate);
		cal.set(Calendar.HOUR_OF_DAY, 11);
		cal.set(Calendar.MINUTE, 30);


		alarmIntent.putExtra(Constants.bundleKeyStart + context.getString(R.string.notification_contactid), friend.getId());


		alarmIntent.putExtra(Constants.bundleKeyStart + context.getString(R.string.notification_contact), friend);
		alarmIntent.putExtra(Constants.bundleKeyStart + context.getString(R.string.notificaiton_event), event);

		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		PendingIntent Sender = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
		alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), DateUtils.HOUR_IN_MILLIS * 2, Sender);
	}


	private static void createAlarmIntent(Context context, FriendsData friend, long date, String event) {
		Intent alarmIntent = new Intent(context, BirthdayNotificationReceiver.class);

		Calendar cal = Calendar.getInstance();
		Date birthdate = getUpcomingBirthday(new Date(date));

		cal.setTime(birthdate);

		alarmIntent.putExtra(Constants.bundleKeyStart + context.getString(R.string.notification_contactid), friend.getId());


		alarmIntent.putExtra(Constants.bundleKeyStart + context.getString(R.string.notification_contact), friend);
		alarmIntent.putExtra(Constants.bundleKeyStart + context.getString(R.string.notificaiton_event), event);

		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		PendingIntent Sender = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), DateUtils.YEAR_IN_MILLIS, Sender);
	}

	public static long getUpcomingDate(long time) {
		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DATE, -1);

		Calendar date = Calendar.getInstance();
		date.setTimeInMillis(time);

		if (yesterday.getTimeInMillis() > time) {
			date.add(Calendar.YEAR, 1);
		}
		return date.getTimeInMillis();
	}

	/**
	 * @param date
	 * 		: Friend's birthdate to get upcoming birthday
	 *
	 * @return date of next birthday.
	 */
	public static Date getUpcomingBirthday(Date date) {

		Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DATE, -1);

		Calendar birthDayThis = Calendar.getInstance();
		birthDayThis.setTime(date);
		birthDayThis.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR));

		if (birthDayThis.before(yesterday)) {
			birthDayThis.add(Calendar.YEAR, 1);
		}
		return birthDayThis.getTime();
	}

	/**
	 * Adds data to calendar
	 */
	public static void addDateToCalendar(FriendsData friend, Context context, long date, String event) {


		startCalendarEvent(friend, context, date, event);

	}

	private static void startCalendarEvent(FriendsData friend, Context context, long date, String event) {


		long startTime = date + 1000 * 60 * 60;
		long endTime = date + 1000 * 60 * 60 * 2;

		Intent intent = new Intent(Intent.ACTION_EDIT);
		intent.setType("vnd.android.cursor.item/event");
		intent.putExtra("title", friend.getName() + "'s birthday");
		intent.putExtra("description", "Event from Wish 'N u");
		intent.putExtra("beginTime", startTime);
		intent.putExtra("endTime", endTime);
		intent.putExtra("allDay", true);
		intent.putExtra("rrule", "FREQ=YEARLY");
		intent.putExtra("hasAlarm", 1);
		context.startActivity(intent);
	}


	public static Intent mailUs(Context context) {
		String[] mails = {context.getString(R.string.contact_us_email)};
		final Intent emailIntent;

		emailIntent = new Intent(Intent.ACTION_SENDTO);
		emailIntent.setData(Uri.parse("mailto:"));
		emailIntent.putExtra(Intent.EXTRA_EMAIL, mails);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));

		return emailIntent;
	}

	public static Intent sendEmailTo(String[] mails) {
		final Intent emailIntent;
		emailIntent = new Intent(Intent.ACTION_SENDTO);
		emailIntent.setData(Uri.parse("mailto:"));
		emailIntent.putExtra(Intent.EXTRA_EMAIL, mails);
		return emailIntent;
	}

	public static Intent spreadUs(Context context) {
		final Intent emailIntent;
		String recoBody = context.getString(R.string.spread_us_messege);

		emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.setType("text/plain");
		String recosubject = context.getString(R.string.sharing_with_you_wish_n_u);
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "" + recosubject);// + " 5th Jan 2010 ");

		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "" + recoBody);

		return emailIntent;
	}

	public static SpannableString getEmptyMessage(Resources resources, String message, int color) {
		String string = new StringBuilder().append(" ").append("\n").append("\n").append(message).toString();
		SpannableString ss = new SpannableString(string);
		Drawable icon = resources.getDrawable(R.drawable.ic_launcher);
		icon.setBounds(0, 0, icon.getIntrinsicWidth(), icon.getIntrinsicHeight());

		ImageSpan span = new ImageSpan(icon, DynamicDrawableSpan.ALIGN_BOTTOM);

		ForegroundColorSpan colorSPan = new ForegroundColorSpan(color);

		ss.setSpan(span, 0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

		ss.setSpan(colorSPan, 0, string.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

		AbsoluteSizeSpan sizeSpan = new AbsoluteSizeSpan(18, true);

		ss.setSpan(sizeSpan, 0, string.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

		return ss;
	}

	public static Intent showPreferences(Context context) {
		return new Intent(context, PreferencesActivity.class);
	}

	public static void getPreferences(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

		String sortingMethod = prefs.getString(context.getString(R.string.sorting_options), "");

		String string = context.getString(R.string.sort_by_birthdays);
		int sort = (sortingMethod.equalsIgnoreCase(context.getString(R.string.no_sorting))) ? Constants.NO_SORTING
		                                                                                    : ((sortingMethod.equalsIgnoreCase(string))
		                                                                                       ? Constants.SORT_BY_BIRTHDATE
		                                                                                       : Constants.SORT_BY_NAME);

		String addAlarmOptions = prefs.getString(context.getString(R.string.add_alarm_options), "");

		String both = context.getString(R.string.both);
		int alarmOptions = (addAlarmOptions.equalsIgnoreCase(both)) ? Constants.BOTH
		                                                            : ((addAlarmOptions.equalsIgnoreCase(context.getString(R.string.set_alarm)))
		                                                               ? Constants.SET_ALARM : Constants.ADD_TO_CALENDAR);

		Constants.SORTING_METHOD = sort;
		Constants.ALARM_OPTIONS = alarmOptions;
	}


	public static void startProgress(Activity activity, MenuItem item) {
		ProgressBar progressBar = new ProgressBar(activity);
		progressBar.setIndeterminate(true);

		MenuItemCompat.setActionView(item, progressBar);
		MenuItemCompat.expandActionView(item);
	}


	public static Picasso getPicasso(Context context) {

		LruCache cache = new LruCache(context);
		return new Builder(context).memoryCache(cache).indicatorsEnabled(BuildConfig.DEBUG).build();
	}

	/**
	 * Extract number from string, failsafe. If the string is not a proper number it will always return 0;
	 *
	 * @param string
	 * 		: String that should be converted into a number
	 *
	 * @return : 0 if conversion to number is failed anyhow, otherwise converted number is returned
	 */
	public static int getNumber(String string) {
		int number = 0;
		if (!isEmptyString(string)) {
			if (TextUtils.isDigitsOnly(string)) {
				number = Integer.parseInt(string);
			}
		}
		return number;
	}

	/**
	 * Is arraylist null or empty
	 *
	 * @param arrayList:
	 * 		ArrayList to check
	 *
	 * @return <code>true</code> is arraylist is null or empty.
	 */
	public static boolean isArrayListEmpty(ArrayList arrayList) {
		return arrayList == null || arrayList.isEmpty();
	}

	public static String prepareInitials(String displayName) {
		String initials = "W";
		if (!isEmptyString(displayName)) {
			if (displayName.contains(" ")) {
				String[] names = displayName.split(" ");

				initials = names[0].substring(0, 1);
				if (names.length > 1) {
					if (!isEmptyString(names[1])) {
						initials += names[1].substring(0, 1);
					}
				}
			} else {
				initials = displayName.substring(0, 1);
			}
		}
		return initials.toUpperCase(Locale.getDefault());
	}

	public static String getDateStringToShow(long date) {
		String formattedDate;
		formattedDate = Constants.FORMAT_SHOW_DATES.format(new Date(date));
		return formattedDate;
	}

	public static void setDailyAlarm(Context context) {
		Intent alarmIntent = new Intent(context, ClearBornTodayReceiver.class);
		Calendar cal = Calendar.getInstance();


		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 10);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		PendingIntent Sender = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), DateUtils.YEAR_IN_MILLIS, Sender);
	}

	public static void applyTintingToImageView(Resources res, ImageView imageView, @ColorRes int color) {
		Drawable drawable = imageView.getDrawable();
		imageView.setImageDrawable(applyTinting(res, drawable, color));
	}

	public static Drawable getTintedDrawable(Resources res, Drawable drawable, @ColorRes int color) {
		return applyTinting(res, drawable, color);
	}

	private static Drawable applyTinting(Resources res, Drawable drawable, @ColorRes int colorRes) {
		int color = res.getColor(colorRes);
		drawable.setColorFilter(color, Mode.SRC_IN);
		return drawable;
	}


}
