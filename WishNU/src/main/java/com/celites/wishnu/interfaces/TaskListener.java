/**
 * 
 */
package com.celites.wishnu.interfaces;

/**
 * @author Prasham
 *
 */
public interface TaskListener {
	
	public void taskFinished(Object o);

}
