/**
 *
 */
package com.celites.wishnu.datautils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;
import com.celites.wishnu.data.ContactType;
import com.celites.wishnu.data.FriendsData;
import com.celites.wishnu.utils.AppLogger;
import com.celites.wishnu.utils.ConstantMethods;
import java.util.ArrayList;

/**
 * @author Prash
 */
public class DatabaseHelper
		extends SQLiteOpenHelper {

	private static final String DBNAME = "WISHNU.db";
	private static final int DATABASE_VERSION = 3;
	private String[] columnNames =
			{DBUtils.KEY_PRIMARYKEY_ID, DBUtils.KEY_ID, DBUtils.KEY_FACEBOOK_ID, DBUtils.KEY_NAME, DBUtils.KEY_BIRTHDAY, DBUtils.KEY_ANNIVERSARY,
			 DBUtils.KEY_PHONE_MAIN, DBUtils.KEY_PHONE_OTHER, DBUtils.KEY_EMAIL_1, DBUtils.KEY_EMAIL_2, DBUtils.KEY_IM_1, DBUtils.KEY_IM_2,
			 DBUtils.KEY_IMAGE, DBUtils.KEY_SHOULD_NOTIFY, DBUtils.KEY_SHOULD_NOTIFY_ANNIVERSARY};
	private String[] dataTypes =
			{"INTEGER", "VARCHAR", "VARCHAR", "VARCHAR", "VARCHAR", "VARCHAR", "VARCHAR", "VARCHAR", "VARCHAR", "VARCHAR", "VARCHAR", "VARCHAR",
			 "VARCHAR", "INTEGER", "INTEGER"};
	private String[] constrains = {"PRIMARY KEY AUTOINCREMENT", "UNIQUE", "", "", "", "", "", "", "", "", "", "", "", "DEFAULT 0", "DEFAULT 0"};
	private SQLiteDatabase db;

	public DatabaseHelper(Context context) {
		super(context, DBNAME, null, DATABASE_VERSION);

	}


	private FriendsData getFriendFromCursor(Cursor friendsCursor) {
		FriendsData friend;
		friend = new FriendsData();
		friend.setId(friendsCursor.getString(friendsCursor.getColumnIndex(DBUtils.KEY_ID)));
		String name = friendsCursor.getString(friendsCursor.getColumnIndex(DBUtils.KEY_NAME));
		friend.setName(name);
		friend.setInitials(ConstantMethods.prepareInitials(name));

		AppLogger.w("Adding", "" + friend.getName());

		setPhoneNumbers(friendsCursor, friend);

		setEmails(friendsCursor, friend);

		setImage(friendsCursor, friend);

		setDates(friendsCursor, friend);

		friend.setShouldNotify(friendsCursor.getInt(friendsCursor.getColumnIndex(DBUtils.KEY_SHOULD_NOTIFY)) != 0);
		friend.setShouldNotifyAnniversary(friendsCursor.getInt(friendsCursor.getColumnIndex(DBUtils.KEY_SHOULD_NOTIFY)) != 0);
		return friend;
	}

	public ArrayList<FriendsData> getFreindsForBirthdayNotification(String tableName){
		if (!db.isOpen()) {
			db = getWritableDatabase();
		}

		ArrayList<FriendsData> friends = new ArrayList<FriendsData>();

		String whereClause=DBUtils.KEY_BIRTHDAY+">0 AND "+DBUtils.KEY_SHOULD_NOTIFY+">0";
		Cursor friendsCursor = db.query(tableName, DBUtils.ALL_COLUMNS, whereClause, null, null, null, null);

		FriendsData friend;

		if (friendsCursor.moveToFirst()) {
			do {
				friend = getFriendFromCursor(friendsCursor);
				friends.add(friend);
			} while (friendsCursor.moveToNext());
			friendsCursor.close();
		}

		return friends;
	}

	public ArrayList<FriendsData> getFreindsForAnniversaryNotification(String tableName) {
		if (!db.isOpen()) {
			db = getWritableDatabase();
		}

		ArrayList<FriendsData> friends = new ArrayList<FriendsData>();

		String whereClause = DBUtils.KEY_ANNIVERSARY + ">0 AND " + DBUtils.KEY_SHOULD_NOTIFY_ANNIVERSARY + ">0";
		Cursor friendsCursor = db.query(tableName, DBUtils.ALL_COLUMNS, whereClause, null, null, null, null);

		FriendsData friend;

		if (friendsCursor.moveToFirst()) {
			do {
				friend = getFriendFromCursor(friendsCursor);
				friends.add(friend);
			} while (friendsCursor.moveToNext());
			friendsCursor.close();
		}

		return friends;
	}

	private void setDates(Cursor mCursor, FriendsData friend) {
		String birthdate = mCursor.getString(mCursor.getColumnIndex(DBUtils.KEY_BIRTHDAY));
		String anniversary = mCursor.getString(mCursor.getColumnIndex(DBUtils.KEY_ANNIVERSARY));

		if (anniversary != null) {
			if (!ConstantMethods.isEmptyString(anniversary) && !anniversary.equalsIgnoreCase("0")) {
				try {
					long milliseconds = Long.parseLong(anniversary);
					friend.setAnniversary(milliseconds);
				} catch (NumberFormatException e) {
					Log.e("Contacts", " DATES : Exception from retreiving from database" + e);
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("Contacts", " DATES : Exception from retreiving from database" + e);
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		if (birthdate != null) {
			if (!ConstantMethods.isEmptyString(birthdate) && !birthdate.equalsIgnoreCase("0")) {
				try {
					long milliseconds = Long.parseLong(birthdate);
					friend.setBirthDate(milliseconds);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					Log.e("Contacts", " DATES : Exception from retreiving from database" + e);
					e.printStackTrace();
				} catch (Exception e) {
					// TODO: handle exception
					Log.e("Contacts", " DATES : Exception from retreiving from database" + e);
					e.printStackTrace();
				}
			}
		}
		friend.setNextDate();
	}

	private void setImage(Cursor mCursor, FriendsData friend) {
		String imageUri = mCursor.getString(mCursor.getColumnIndex(DBUtils.KEY_IMAGE));
		friend.setImageUri(imageUri);
	}

	private void setEmails(Cursor mCursor, FriendsData friend) {
		String email1 = mCursor.getString(mCursor.getColumnIndex(DBUtils.KEY_EMAIL_1));
		String email2 = mCursor.getString(mCursor.getColumnIndex(DBUtils.KEY_EMAIL_2));

		ArrayList<String> emails = new ArrayList<String>();
		if (!ConstantMethods.isEmptyString(email1)) emails.add(email1);
		if (!ConstantMethods.isEmptyString(email2)) emails.add(email2);
		friend.setEmails(emails);
	}

	private void setPhoneNumbers(Cursor mCursor, FriendsData friend) {
		String phoneNumber1 = mCursor.getString(mCursor.getColumnIndex(DBUtils.KEY_PHONE_MAIN));
		String phoneNumber2 = mCursor.getString(mCursor.getColumnIndex(DBUtils.KEY_PHONE_OTHER));

		ArrayList<String> phoneNumbers = new ArrayList<String>();
		if (!ConstantMethods.isEmptyString(phoneNumber1)) phoneNumbers.add(phoneNumber1);
		if (!ConstantMethods.isEmptyString(phoneNumber2)) phoneNumbers.add(phoneNumber2);
		friend.setPhoneNumbers(phoneNumbers);
	}

	private ContentValues createContextValues(FriendsData friend) {
		ContentValues values = new ContentValues();


		values.put(DBUtils.KEY_ID, friend.getId());
		values.put(DBUtils.KEY_NAME, friend.getName());

		try {
			values.put(DBUtils.KEY_IMAGE, friend.getImageUri());
		} catch (Exception e1) {
			values.put(DBUtils.KEY_IMAGE, "");
			e1.printStackTrace();
		}

		ArrayList<String> phoneNumbers = friend.getPhoneNumbers();
		if (phoneNumbers != null) {
			for (int i = 0; i < phoneNumbers.size(); i++) {
				try {
					if (i == 2) break;
					String email = phoneNumbers.get(i);
					if (i == 0) values.put(DBUtils.KEY_PHONE_MAIN, email);
					if (i == 1) values.put(DBUtils.KEY_PHONE_OTHER, email);
				} catch (Exception e) {
					String email = "";
					values.put(DBUtils.KEY_PHONE_MAIN, email);
					values.put(DBUtils.KEY_PHONE_OTHER, email);
				}
			}
		} else {
			String email = "";
			values.put(DBUtils.KEY_EMAIL_1, email);
			values.put(DBUtils.KEY_EMAIL_2, email);
		}

		ArrayList<String> emails = friend.getEmails();
		if (emails != null) {
			for (int i = 0; i < emails.size(); i++) {
				try {
					if (i == 2) break;
					String email = emails.get(i);
					if (i == 0) values.put(DBUtils.KEY_EMAIL_1, email);
					if (i == 1) values.put(DBUtils.KEY_EMAIL_2, email);
				} catch (Exception e) {
					String email = "";
					values.put(DBUtils.KEY_EMAIL_1, email);
					values.put(DBUtils.KEY_EMAIL_2, email);
					// TODO Auto-generated catch block
				}
			}
		} else {
			String email = "";
			values.put(DBUtils.KEY_EMAIL_1, email);
			values.put(DBUtils.KEY_EMAIL_2, email);
		}

		try {
			long birthDate = friend.getBirthDate();
			values.put(DBUtils.KEY_BIRTHDAY, "" + birthDate);
			long anniversary = friend.getAnniversary();
			values.put(DBUtils.KEY_ANNIVERSARY, "" + anniversary);

		} catch (Exception e) {

			Log.e("Contacts", " DATES : Exception in adding the database" + e);

			// TODO Auto-generated catch block
			//			values.put(DBUtils.KEY_BIRTHDAY, "" + 0);
			//
			//			values.put(DBUtils.KEY_ANNIVERSARY, "" + 0);

		}

		boolean shouldNotify = friend.isShouldNotify();
		int shouldNotifyBirthday = shouldNotify ? 1 : 0;
		boolean shouldNotifyAnniv = friend.isShouldNotifyAnniversary();
		int shouldNotifyAnniversary = shouldNotifyAnniv ? 1 : 0;

		values.put(DBUtils.KEY_SHOULD_NOTIFY, shouldNotifyBirthday);
		values.put(DBUtils.KEY_SHOULD_NOTIFY_ANNIVERSARY, shouldNotifyAnniversary);

		return values;
	}


	/**
	 * Creates table in given database
	 *
	 * @param database
	 * 		: {@link SQLiteDatabase} Database in which the table needs to be created
	 * @param tableName
	 * 		: Name of table
	 * @param columnNames
	 * 		: String array of column names
	 * @param dataTypes
	 * 		: String array of Datatypes for the columns, (Pass the length of the column data here like VARCHAR(15))
	 * @param constrains
	 * 		: Constrains for columns, leave null if no Constrains needed to be applied in columns
	 *
	 * @throws ImproperTablesArgumentsException
	 * 		: When column names, datatype and constrains have different numbers of elements
	 */
	void createTable(SQLiteDatabase database, String tableName, String[] columnNames, String[] dataTypes, String[] constrains) throws
	                                                                                                                           ImproperTablesArgumentsException {

		if (database == null) {

			try {
				database = getWritableDatabase();
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		if (columnNames.length != dataTypes.length && dataTypes.length != constrains.length) {
			throw new ImproperTablesArgumentsException();
		} else {
			StringBuilder query = new StringBuilder("CREATE TABLE IF NOT EXISTS " + tableName + " (");

			for (int i = 0; i < columnNames.length; i++) {
				query.append(columnNames[i]);
				query.append(" ");
				query.append(dataTypes[i]);
				if (constrains[i] != null && constrains[i].length() > 0 && !constrains[i].equalsIgnoreCase("")) {
					query.append(" ");
					query.append(constrains[i]);
				}
				if (i != columnNames.length - 1) query.append(",");
			}
			query.append(")");

			String createTableQuery = query.toString();

			Log.i("Create Table Query", "" + createTableQuery);

			Log.w("Contacts", "Contacts " + createTableQuery);

			if (database != null) {
				database.execSQL(createTableQuery);
			}

		}

	}

	void createTables() {
		try {

			if (db == null) this.getWritableDatabase();
			createTable(db, DBUtils.CONTACTS_TABLE_NAME, columnNames, dataTypes, constrains);
			createTable(db, DBUtils.FACEBOOK_TABLE_NAME, columnNames, dataTypes, constrains);
			createTable(db, DBUtils.GOOGLE_TABLE_NAME, columnNames, dataTypes, constrains);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ImproperTablesArgumentsException e) {
			// TODO Auto-generated catch block
			Log.w("test", "Imporper table" + e);
		}

	}

	public ArrayList<FriendsData> getAllFriends(String tableName) {

		if (!db.isOpen()) {
			db = getWritableDatabase();
		}

		ArrayList<FriendsData> friends = new ArrayList<FriendsData>();
		Cursor friendsCursor = db.query(tableName, DBUtils.ALL_COLUMNS, null, null, null, null, null);

		FriendsData friend;

		if (friendsCursor.moveToFirst()) {
			do {
				friend = getFriendFromCursor(friendsCursor);
				friends.add(friend);
			} while (friendsCursor.moveToNext());
			friendsCursor.close();
		}

		return friends;
	}

	public int getCount(String tableName) {
		Cursor cr = db.query(tableName, DBUtils.ALL_COUNTS, null, null, null, null, null);
		if (cr.moveToFirst()) {
			int count = cr.getInt(0);
			cr.close();
			return count;
		}
		return 0;
	}

	public SQLiteDatabase getDb() {
		return db;
	}

	public FriendsData getFirstFriendOfType(int contactType) {
		String tableToFetch = null;
		ContactType friendType = ContactType.ADDRESS_BOOK;
		@SuppressWarnings("UnnecessaryLocalVariable") int type = contactType;
		if (type == ContactType.ADDRESS_BOOK.ordinal()) {
			tableToFetch = DBUtils.CONTACTS_TABLE_NAME;
			friendType = ContactType.ADDRESS_BOOK;
		} else if (type == ContactType.FACEBOOK.ordinal()) {
			tableToFetch = DBUtils.FACEBOOK_TABLE_NAME;
			friendType = ContactType.FACEBOOK;
		} else if (type == ContactType.GOOGLE.ordinal()) {
			tableToFetch = DBUtils.GOOGLE_TABLE_NAME;
			friendType = ContactType.GOOGLE;
		}
		Cursor friendsCursor = null;
		if (tableToFetch != null) {
			friendsCursor = db.query(tableToFetch, DBUtils.ALL_COLUMNS, null, null, null, null, null);
		}
		if (friendsCursor != null) {
			if (friendsCursor.moveToFirst()) {
				FriendsData friend = getFriendFromCursor(friendsCursor);
				friend.setType(friendType);
				return friend;
			}
		}
		return null;
	}

	public FriendsData getFriendOfType(int type, String id) {
		String tableToFetch = null;
		ContactType friendType = ContactType.ADDRESS_BOOK;
		if (TextUtils.isEmpty(id)) {
			return null;
		} else {
			if (type == ContactType.ADDRESS_BOOK.ordinal()) {
				tableToFetch = DBUtils.CONTACTS_TABLE_NAME;
				friendType = ContactType.ADDRESS_BOOK;
			} else if (type == ContactType.FACEBOOK.ordinal()) {
				tableToFetch = DBUtils.FACEBOOK_TABLE_NAME;
				friendType = ContactType.FACEBOOK;
			} else if (type == ContactType.GOOGLE.ordinal()) {
				tableToFetch = DBUtils.GOOGLE_TABLE_NAME;
				friendType = ContactType.GOOGLE;
			}
			Cursor friendsCursor = null;
			if (tableToFetch != null) {
				friendsCursor = db.query(tableToFetch, DBUtils.ALL_COLUMNS, DBUtils.KEY_ID + " = " + id, null, null, null, null);
			}
			if (friendsCursor != null) {
				if (friendsCursor.moveToFirst()) {
					FriendsData friend = getFriendFromCursor(friendsCursor);
					friend.setType(friendType);
					return friend;
				}
			}
		}
		return null;
	}

	public long insert(String tableName, FriendsData friend) {
		ContentValues insertValues = createContextValues(friend);

		if (db == null || !db.isOpen()) db = getWritableDatabase();

		String whereClause = DBUtils.KEY_ID + "=" + friend.getId();
		Cursor friendsCursor = db.query(tableName, new String[]{DBUtils.KEY_ID}, whereClause, null, null, null, null);
		long returnValue = -1;
		AppLogger.v("Update Clause ", "String where Clause " + whereClause + " Table Name " + tableName);
		try {
			if (friendsCursor.moveToFirst()) {
				@SuppressWarnings("UnnecessaryLocalVariable") long updateKey = db.update(tableName, insertValues, whereClause, null);
				returnValue = updateKey;
			} else {

				@SuppressWarnings("UnnecessaryLocalVariable") long insert = db.insert(tableName, null, insertValues);
				returnValue = insert;
			}
		} finally {
			db.close();
		}

		return returnValue;
	}

	public boolean isOpen() {
		return db != null && db.isOpen();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seepublic void getFirstFriendOfType(ContactType book) {
	}
	 * android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite
	 * .SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		this.db = db;
		createTables();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite
	 * .SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		this.db = db;
		if (oldVersion == 1) {
			if (newVersion == 2) {
				//THIS SHUOLD NEVER HAPPEN AS 2 IS ONLY USED FOR INTERNAL PURPOSE AND IN ONLY ONE DEVICE.
				db.execSQL("ALTER TABLE " + DBUtils.CONTACTS_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY + " INTEGER DEFAULT 0");
				db.execSQL("ALTER TABLE " + DBUtils.FACEBOOK_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY + " INTEGER DEFAULT 0");
				db.execSQL("ALTER TABLE " + DBUtils.GOOGLE_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY + " INTEGER DEFAULT 0");
			} else if (newVersion == 3) {
				db.execSQL("ALTER TABLE " + DBUtils.CONTACTS_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY + " INTEGER DEFAULT 0");
				db.execSQL("ALTER TABLE " + DBUtils.FACEBOOK_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY + " INTEGER DEFAULT 0");
				db.execSQL("ALTER TABLE " + DBUtils.GOOGLE_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY + " INTEGER DEFAULT 0");

				db.execSQL("ALTER TABLE " + DBUtils.CONTACTS_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY_ANNIVERSARY + " INTEGER DEFAULT 0");
				db.execSQL("ALTER TABLE " + DBUtils.FACEBOOK_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY_ANNIVERSARY + " INTEGER DEFAULT 0");
				db.execSQL("ALTER TABLE " + DBUtils.GOOGLE_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY_ANNIVERSARY + " INTEGER DEFAULT 0");
			}
		}else if (oldVersion==2){
			//THIS SHUOLD NEVER HAPPEN AS 2 IS ONLY USED FOR INTERNAL PURPOSE AND IN ONLY ONE DEVICE.
			if(newVersion==3){
				db.execSQL("ALTER TABLE " + DBUtils.CONTACTS_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY_ANNIVERSARY +
				           " INTEGER DEFAULT 0");
				db.execSQL("ALTER TABLE " + DBUtils.FACEBOOK_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY_ANNIVERSARY +
				           " INTEGER DEFAULT 0");
				db.execSQL("ALTER TABLE " + DBUtils.GOOGLE_TABLE_NAME + " ADD COLUMN " + DBUtils.KEY_SHOULD_NOTIFY_ANNIVERSARY +
				           " INTEGER DEFAULT 0");
			}
		}

	}

	public void close() {
		if (db != null) {
			db.close();
		}
	}

	public void open() {

		if (db == null || (!db.isOpen() && !db.isDbLockedByCurrentThread())) {
			this.db = getWritableDatabase();
		}
	}

	public void setSortingMethod(int sortingMethod) {
		DBUtils.SORTING_METHOD = sortingMethod;
	}

}
