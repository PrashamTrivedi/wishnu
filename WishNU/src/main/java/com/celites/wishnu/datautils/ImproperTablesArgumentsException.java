/**
 * 
 */
package com.celites.wishnu.datautils;


/**
 * @author Prasham Trivedi
 * 
 */
public class ImproperTablesArgumentsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4032948070499723775L;

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "com.celites.wishnu.datautils.ImproperTablesArgumentsException column names array, datatypes array and arguments array must have same number of elements in it";
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "names array, datatypes array and arguments array must have same number of elements in it";
	}

}
