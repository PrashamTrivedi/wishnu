/**
 * 
 */
package com.celites.wishnu.datautils;

/**
 * @author Owner
 * 
 */
public class DBUtils {

	public static final String CONTACTS_TABLE_NAME = "CONTACTS";
	public static final String FACEBOOK_TABLE_NAME = "FACEBOOK";
	public static final String GOOGLE_TABLE_NAME = "GOOGLE";

	public static final String KEY_PRIMARYKEY_ID = "_ID";
	public static final String KEY_ID = "ID";
	public static final String KEY_NAME = "NAME";
	public static final String KEY_BIRTHDAY = "BIRTHDAY";
	public static final String KEY_ANNIVERSARY = "ANNIVERSARY";
	public static final String KEY_FACEBOOK_ID = "FACEBOOK_ID";
	public static final String KEY_EMAIL_1 = "EMAIL1";
	public static final String KEY_EMAIL_2 = "EMAIL2";
	public static final String KEY_PHONE_MAIN = "PHONE";
	public static final String KEY_PHONE_OTHER = "PHONE2";
	public static final String KEY_IM_1 = "IM1";
	public static final String KEY_IM_2 = "IM2";
	public static final String KEY_IMAGE = "IMAGE";

	public static final String KEY_SHOULD_NOTIFY = "SHOULD_NOTIFY";
	public static final String KEY_SHOULD_NOTIFY_ANNIVERSARY = "SHOULD_NOTIFY_ANNIVERSARY";


	static final String[] ALL_COLUMNS = { "*" };
	static final String[] ALL_COUNTS = { "COUNT (*)" };

	public static int SORTING_METHOD;

}
