/**
 *
 */
package com.celites.wishnu;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Action;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.util.TypedValue;
import com.celites.wishnu.customViews.CharacterDrawable;
import com.celites.wishnu.data.FriendsData;
import com.celites.wishnu.utils.AnalyticsLogger;
import com.celites.wishnu.utils.ConstantMethods;
import com.celites.wishnu.utils.Constants;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;
import java.util.ArrayList;
import java.util.Locale;

/**
 * @author Prash
 */
public class BirthdayNotificationReceiver
		extends WakefulBroadcastReceiver {

	Bitmap image;

	@Override
	public void onReceive(final Context context, Intent intent) {
		final NotificationManager manager = (NotificationManager) context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);

		Intent wishOnWallIntent = new Intent(context, BaseActivity.class);
		wishOnWallIntent.setAction("wish");
		Bundle extras = intent.getExtras();
		if (extras != null) {
			final FriendsData friend = extras.getParcelable(Constants.bundleKeyStart + context.getString(R.string.notification_contact));
			final String id = friend.getId();
			final String event = extras.getString(Constants.bundleKeyStart + context.getString(R.string.notificaiton_event));

			final String name = friend.getName();

			wishOnWallIntent.putExtras(extras);

			final PendingIntent wishIntent = PendingIntent.getActivity(context, 0, wishOnWallIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			String uri = friend.getImageUri();
			if (ConstantMethods.isEmptyString(uri)) {
				CharacterDrawable drawable = new CharacterDrawable(friend.getInitials(), context.getResources().getColor(R.color.app_purple));
				image = drawableToBitmap(context, drawable);
			} else {

				Picasso.with(context).load(uri).into(new Target() {
					@Override
					public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
						image = bitmap;
					}

					@Override
					public void onBitmapFailed(Drawable errorDrawable) {

					}

					@Override
					public void onPrepareLoad(Drawable placeHolderDrawable) {

					}
				});
			}

			AnalyticsLogger analyticsLogger = AnalyticsLogger.init(context);

			ArrayList<Action> actionsAvailable = new ArrayList<Action>();



			String phoneNo = friend.getPhoneNumbers().get(0);
			if (!ConstantMethods.isEmptyString(phoneNo)) {
				Intent call= new Intent(context, NotificationActionReceiver.class);
				call.putExtra("data", phoneNo);
				call.setAction("CALL");
				PendingIntent callIntent = PendingIntent.getBroadcast(context, 0, call, 0);
				Action callAction = new Action(R.drawable.ic_notification_call, context.getString(R.string.call_number), callIntent);
				actionsAvailable.add(callAction);

				Intent sms = new Intent(context, NotificationActionReceiver.class);
				sms.putExtra("data", phoneNo);
				sms.setAction("SMS");
				PendingIntent smsIntent = PendingIntent.getBroadcast(context, 0, sms, 0);
				Action smsAction = new Action(R.drawable.ic_notification_sms, context.getString(R.string.send_sms), smsIntent);
				actionsAvailable.add(smsAction);
			}

			String contentText = String.format(Locale.getDefault(), context.getString(R.string.notificationMessage), name, event);
			Builder builder = new Builder(context);
			NotificationCompat.Builder notificationBuilder =
					builder.setSmallIcon(R.drawable.ic_notification).setContentTitle(context.getString(R.string.app_name)).setContentText(contentText)
					       .setContentIntent(wishIntent).setLargeIcon(image);
			long alarmConversation = Long.parseLong(id);

			if (!ConstantMethods.isArrayListEmpty(actionsAvailable)) {
				for (Action action : actionsAvailable) {
					builder.addAction(action);
				}
			}

			Integer alarmId = (int) alarmConversation;

			Notification notification = notificationBuilder.build();

			notification.flags = Notification.FLAG_AUTO_CANCEL;

			manager.notify(alarmId, notification);

		}
	}

	public static Bitmap drawableToBitmap(Context context, Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable) drawable).getBitmap();
		}

		int width = drawable.getIntrinsicWidth();
		int height = drawable.getIntrinsicHeight();
		Resources r = context.getResources();
		int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 56.0f, r.getDisplayMetrics());
		if (width <= 0) width = px;
		if (height <= 0) height = px;
		Log.d("dimensions", "Dimensions " + width + " , " + height);
		Bitmap bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);

		return bitmap;
	}

}
