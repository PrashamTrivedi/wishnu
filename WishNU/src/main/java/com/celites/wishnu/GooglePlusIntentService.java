package com.celites.wishnu;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import com.celites.wishnu.data.FriendsData;
import com.celites.wishnu.datautils.DBUtils;
import com.celites.wishnu.datautils.DatabaseHelper;
import com.celites.wishnu.utils.AppLogger;
import com.celites.wishnu.utils.Constants;
import java.util.ArrayList;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class GooglePlusIntentService extends IntentService {

	private DatabaseHelper dbHelper;

	public GooglePlusIntentService() {
        super("GooglePlusIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
	        //TODO : Moove Google + handling here.
	        ArrayList<FriendsData> people =  intent.getParcelableArrayListExtra("People");
	        addGooglePlusContatsToDatabase(people);
        }
    }

	public void addGooglePlusContatsToDatabase(ArrayList<FriendsData> people) {
		if (dbHelper == null) {
			dbHelper = new DatabaseHelper(this);
			dbHelper.open();
		}
		for (FriendsData friend : people) {

			AppLogger.v("GooglePlus","Loading "+friend);

			try {
				dbHelper.insert(DBUtils.GOOGLE_TABLE_NAME, friend);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		sendFinishedBroadcast();

	}

	private void sendFinishedBroadcast() {
		// TODO Auto-generated method stub
		Intent notifierIntent = new Intent(Constants.ACTION_GOOGLE_LOADED);
		LocalBroadcastManager.getInstance(this).sendBroadcast(notifierIntent);
	}


}
