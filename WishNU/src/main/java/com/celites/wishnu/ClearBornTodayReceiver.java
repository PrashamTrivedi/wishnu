package com.celites.wishnu;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import com.celites.wishnu.utils.AppExternalFileWriter;
import com.celites.wishnu.utils.AppExternalFileWriter.ExternalFileWriterException;
import java.io.File;

public class ClearBornTodayReceiver
		extends WakefulBroadcastReceiver {
	public ClearBornTodayReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO: This method is called when the BroadcastReceiver is receiving
		try {
			AppExternalFileWriter writer = new AppExternalFileWriter(context);
			File bornTodays = writer.createSubDirectory("BornToday", true);
			writer.writeDataToFile(bornTodays, "BornToday", "");
		} catch (ExternalFileWriterException e) {
			e.printStackTrace();
		}
	}
}
