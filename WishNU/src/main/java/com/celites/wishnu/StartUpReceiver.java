package com.celites.wishnu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartUpReceiver extends BroadcastReceiver {
    public StartUpReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
	    Intent prepareNotificationService = new Intent(context, PrepareForNotificationService.class);
	    context.startService(prepareNotificationService);
    }
}
