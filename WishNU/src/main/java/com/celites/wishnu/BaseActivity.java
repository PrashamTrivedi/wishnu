/**
 *
 */
package com.celites.wishnu;

import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.ShareActionProvider.OnShareTargetSelectedListener;
import android.view.Menu;
import android.view.MenuItem;
import com.ceelites.sharedpreferenceinspector.Utils.SharedPreferenceUtils;
import com.celites.wishnu.data.FriendsData;
import com.celites.wishnu.datautils.DatabaseHelper;
import com.celites.wishnu.fragments.CelebretiesFragment;
import com.celites.wishnu.fragments.ContactsListFragment;
import com.celites.wishnu.fragments.FriendsDetailFragment;
import com.celites.wishnu.fragments.GooglePlusFragment;
import com.celites.wishnu.utils.AnalyticsLogger;
import com.celites.wishnu.utils.AppLogger;
import com.celites.wishnu.utils.ConstantMethods;
import com.celites.wishnu.utils.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import java.util.ArrayList;

/**
 * @author Prasham
 */
public class BaseActivity
		extends ActionBarActivity
		implements ConnectionCallbacks, OnConnectionFailedListener {

	private static int currentFragment = 1;
	private final int REQUEST_CODE_SIGN_IN = 1234;
	ArrayList<FriendsData> people = new ArrayList<FriendsData>();
	private DatabaseHelper dbHelper;
	private SharedPreferences prefs;
	private ContactsListFragment contactListFragment;
	private GooglePlusFragment googlePlusFragment;
	private TabListener contactSelectedListener = new TabListener() {
		@Override
		public void onTabSelected(Tab tab, FragmentTransaction transaction) {
			ContactsListFragment fragment = ContactsListFragment.newInstance();
			getSupportFragmentManager().beginTransaction().replace(R.id.contentFrame, fragment).commit();
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction transaction) {

		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction transaction) {

		}
	};
	private TabListener celebritySelectedListener = new TabListener() {
		@Override
		public void onTabSelected(Tab tab, FragmentTransaction transaction) {
			CelebretiesFragment fragment = CelebretiesFragment.newInstance(null);
			getSupportFragmentManager().beginTransaction().replace(R.id.contentFrame, fragment).commit();
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction transaction) {

		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction transaction) {

		}
	};
	private TabListener googlePlusSelectedListener = new TabListener() {
		@Override
		public void onTabSelected(Tab tab, FragmentTransaction transaction) {
			GooglePlusFragment fragment = GooglePlusFragment.newInstance();
			getSupportFragmentManager().beginTransaction().replace(R.id.contentFrame, fragment).commit();
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction transaction) {

		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction transaction) {

		}
	};
	private GoogleApiClient googleApiClient;
	private boolean isIntentInProgress;
	private ActionBar actionBar;
	private Person currentPerson;
	private String nextPageToken;
	private long startTime;
	private AnalyticsLogger analyticsLogger;
	private SharedPreferenceUtils prefsUtils;

	private void openFragment(Fragment fragment, String tag) {
		FragmentManager manager = getSupportFragmentManager();
		manager.beginTransaction().replace(R.id.contentFrame, fragment, tag).commit();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == REQUEST_CODE_SIGN_IN) {
			if (resultCode == RESULT_OK && !googleApiClient.isConnected() && !googleApiClient.isConnecting()) {
				// This time, connect should succeed.
				googleApiClient.connect();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		checkAndWishFriend(intent);
		super.onNewIntent(intent);
	}

	private void checkAndWishFriend(Intent intent) {
		String action = intent.getAction();
		if (action != null) {
			if (action.equalsIgnoreCase("wish")) {
				FriendsDetailFragment friendsDetailFragment = new FriendsDetailFragment();
				Bundle bundle = new Bundle();

				Bundle bundleFromIntent = intent.getExtras();

				String id = null;
				int type = 0;

				FriendsData friend = intent.getParcelableExtra(Constants.bundleKeyStart + getString(R.string.notification_contact));

				if (friend != null) {
					bundle.putParcelable(getString(R.string.friends_extra), friend);
					friendsDetailFragment.setArguments(bundle);
					getSupportFragmentManager().beginTransaction().replace(R.id.contentFrame, friendsDetailFragment, FriendsDetailFragment.TAG)
					                           .commit();
				}
			}
		}
	}

	/**
	 * @return the dbHelper
	 */
	public DatabaseHelper getDbHelper() {
		return dbHelper;
	}

	/**
	 * @param dbHelper
	 * 		the dbHelper to set
	 */
	public void setDbHelper(DatabaseHelper dbHelper) {
		this.dbHelper = dbHelper;
	}

	public GoogleApiClient getGoogleApiClient() {
		return googleApiClient;
	}

	/**
	 * @return the prefs
	 */
	public SharedPreferences getPrefs() {
		return prefs;
	}

	/**
	 * @param prefs
	 * 		the prefs to set
	 */
	public void setPrefs(SharedPreferences prefs) {
		this.prefs = prefs;
	}

	public void onConnected(Bundle bundle) {
		currentPerson = Plus.PeopleApi.getCurrentPerson(googleApiClient);
		ResultCallback<LoadPeopleResult> callback = new ResultCallback<LoadPeopleResult>() {
			@Override
			public void onResult(LoadPeopleResult result) {

				if (result.getStatus().getStatusCode() == CommonStatusCodes.SUCCESS) {
					nextPageToken = result.getNextPageToken();

					addToPeople(result, people);
					people.trimToSize();
					AppLogger.d("Google Pls", "Person Buffer NextPageToken = " + nextPageToken);
					if (!ConstantMethods.isEmptyString(nextPageToken)) {
						Plus.PeopleApi.loadVisible(googleApiClient, nextPageToken).setResultCallback(this);
					} else {
						startGooglePlusService(people);
					}
				} else {
					Intent notifierIntent = new Intent(Constants.ACTION_GOOGLE_LOADED);
					LocalBroadcastManager.getInstance(BaseActivity.this).sendBroadcast(notifierIntent);
				}
			}

			private void addToPeople(LoadPeopleResult result, ArrayList<FriendsData> people) {
				PersonBuffer personBuffer = result.getPersonBuffer();
				int count = personBuffer.getCount();
				AppLogger.d("Google Pls", "Person Buffer Count = " + count);
				for (int i = 0; i < count; i++) {

					Person person = personBuffer.get(i);

					FriendsData friend = FriendsData.createFrom(person);

					people.add(friend);
				}
			}
		};
		startTime = System.currentTimeMillis();

		Plus.PeopleApi.loadVisible(googleApiClient, null).setResultCallback(callback);
	}

	@Override
	public void onConnectionSuspended(int i) {

	}

	public void startGooglePlusService(ArrayList<FriendsData> people) {
		long endTime = System.currentTimeMillis();
		long diff = endTime - startTime;
		startTime = 0;
		getAnalyticsLogger().sendTime("Loading", "GooglePlus", "Fetching (" + people.size() + ")", diff);
		Intent googlePlusServiceIntent = new Intent(this, GooglePlusIntentService.class);
		googlePlusServiceIntent.putParcelableArrayListExtra("People", people);
		startService(googlePlusServiceIntent);
	}

	public AnalyticsLogger getAnalyticsLogger() {
		if (analyticsLogger == null) {
			analyticsLogger = AnalyticsLogger.init(this);
			analyticsLogger.askUserConsent(this);
		}
		return analyticsLogger;
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (!isIntentInProgress && result.hasResolution()) {
			try {
				isIntentInProgress = true;
				result.startResolutionForResult(this, REQUEST_CODE_SIGN_IN);
			} catch (SendIntentException e) {
				// The intent was canceled before it was sent.  Return to the default
				// state and attempt to connect to get an updated ConnectionResult.
				isIntentInProgress = false;
				googleApiClient.connect();
			}
		}
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_base);
		AnalyticsLogger logger = getAnalyticsLogger();


		logger.sendScreenData();

		actionBar = getSupportActionBar();

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		actionBar.addTab(actionBar.newTab().setText(R.string.menu_contacts).setTabListener(contactSelectedListener));
		actionBar.addTab(actionBar.newTab().setText(R.string.menu_google_plus).setTabListener(googlePlusSelectedListener));
		actionBar.addTab(actionBar.newTab().setText(R.string.menu_born_today).setTabListener(celebritySelectedListener));

		ConstantMethods.getPreferences(this);


		openDatabase();

		initGooglePlus();

		checkAndWishFriend(getIntent());

		ConstantMethods.setDailyAlarm(this);

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefsUtils = SharedPreferenceUtils.initWith(this, null);
		SharedPreferences anotherFrekingPref = getSharedPreferences("Prasham", MODE_PRIVATE);
		try {
			String resetAlarms = prefsUtils.getString(Constants.RESET_ALARMS, "");
			if (ConstantMethods.isEmptyString(resetAlarms)) {
				Intent prepareNotificationService = new Intent(this, PrepareForNotificationService.class);
				this.startService(prepareNotificationService);
				prefsUtils.putString(Constants.RESET_ALARMS, "Done");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		anotherFrekingPref.edit().putString("Test", "Love you life").commit();
		anotherFrekingPref.edit().putInt("Test Int", 2488).commit();
	}

	@Override
	protected void onStop() {
		if (googleApiClient.isConnected()) googleApiClient.disconnect();
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		Crouton.cancelAllCroutons();
		dbHelper.close();
		dbHelper = null;
		super.onDestroy();
	}

	private void openDatabase() {
		if (dbHelper == null) {
			dbHelper = new DatabaseHelper(this);
			dbHelper.open();
		}
	}

	private void initGooglePlus() {
		googleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(Plus.API)
		                                                   .addScope(Plus.SCOPE_PLUS_LOGIN).addScope(Plus.SCOPE_PLUS_PROFILE).build();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		MenuItem shareItem = menu.findItem(R.id.spreadUs);
		ShareActionProvider provider = null;
		if (shareItem != null) {
			provider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
		}
		if (provider != null) {
			provider.setShareHistoryFileName("test.xml");
			provider.setShareIntent(ConstantMethods.spreadUs(this));
			provider.setOnShareTargetSelectedListener(new OnShareTargetSelectedListener() {
				@Override
				public boolean onShareTargetSelected(ShareActionProvider provider, Intent intent) {
					getAnalyticsLogger().sendEvent("MenuItem", "Share", "Sharing to " + intent.getComponent().getPackageName());
					return false;
				}
			});
		}

		prefsUtils.inflateDebugMenu(getMenuInflater(), menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (!prefsUtils.isDebugHandled(this, item)) {
			int id = item.getItemId();
			if (id == R.id.contactUs) {
				startActivity(ConstantMethods.mailUs(this));
				AnalyticsLogger.init(this).sendEvent("MenuItem", "Contact Us", "");
			}
		}
		//		if (id == R.id.action_debug) {
		//
		//		}

		return super.onOptionsItemSelected(item);
	}
}
