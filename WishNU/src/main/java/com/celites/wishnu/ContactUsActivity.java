/**
 *
 */
package com.celites.wishnu;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;
import com.celites.wishnu.utils.ConstantMethods;
import de.keyboardsurfer.android.widget.crouton.Crouton;

/**
 * @author Prasham
 */
public class ContactUsActivity
		extends ActionBarActivity {

	@Override
	protected void onDestroy() {
		Crouton.cancelAllCroutons();
		super.onDestroy();
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contactusmenulayout);
		TextView emailUs = (TextView) findViewById(R.id.email);
		emailUs.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ConstantMethods.mailUs(ContactUsActivity.this);
			}
		});

		TextView facebookPage = (TextView) findViewById(R.id.facebook);
		facebookPage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(ContactUsActivity.this,
				               "Facebook is about to be integrated soon, Please wait",
				               Toast.LENGTH_LONG).show();
				// TODO Auto-generated method stub

			}
		});
	}
}
