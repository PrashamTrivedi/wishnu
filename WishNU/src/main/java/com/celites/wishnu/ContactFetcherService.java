package com.celites.wishnu;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Contacts.Photo;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.celites.wishnu.data.FriendsData;
import com.celites.wishnu.datautils.DBUtils;
import com.celites.wishnu.datautils.DatabaseHelper;
import com.celites.wishnu.utils.AnalyticsLogger;
import com.celites.wishnu.utils.AppLogger;
import com.celites.wishnu.utils.ConstantMethods;
import com.celites.wishnu.utils.Constants;
import java.text.ParseException;

/**
 * An {@link android.app.IntentService} subclass for handling asynchronous task requests in a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static helper methods.
 */
public class ContactFetcherService
		extends IntentService {


	private DatabaseHelper dbHelper;

	public ContactFetcherService() {
		super("ContactIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent != null) {
			long startTime = System.currentTimeMillis();
			ContentResolver cr = getContentResolver();
			Cursor cursor = null;
			int i = 0;
			if (ContactsContract.Contacts.CONTENT_URI != null) {
				cursor = cr.query(Contacts.CONTENT_URI, null, null, null, null);
				i = 0;
				if (cursor.moveToFirst()) {

					do {

						FriendsData friend = new FriendsData();

						String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
						String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

						AppLogger.w("Contacts", "Contacts Module = id = " + id + " Name = " + name);

						friend.setName(name);
						friend.setInitials(ConstantMethods.prepareInitials(name));
						friend.setId(id);

						// get phone numbers
						getPhoneNumberDetails(cr, cursor, id, friend);

						// get emails
						getEmailDetails(cr, id, friend);


						// Get picture details
						openPhoto(cr, cursor, id, friend);

						// Get Birthdate and anniversary details
						getDatesDetails(cr, id, friend);

						addToDatabase(friend);
						i++;

					} while (cursor.moveToNext());

					cursor.close();

				}
			}
			long endtime = System.currentTimeMillis();

			long difference = endtime - startTime;

			AnalyticsLogger.init(this).sendTime("Loading", "Contact Loading", "Refresh (" + i + " contacts) ", difference);

			sendFinishedBroadcast();
		}
	}

	private void getPhoneNumberDetails(ContentResolver cr, Cursor cursor, String id, FriendsData friend) {
		if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
			Cursor pCur = null;
			if (ContactsContract.CommonDataKinds.Phone.CONTENT_URI != null) {
				pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
				                new String[]{id}, null);

				int count = pCur.getCount();
				if (pCur.moveToFirst()) {
					do {
						String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

						String isPrimary = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.IS_PRIMARY));

						Log.w("Contacts", "Contacts Module = number = " + phone + " isPrimary = " + isPrimary);

						boolean isPrimaryPhoneNumber = isPrimary.equalsIgnoreCase("1");
						if (isPrimaryPhoneNumber) {
							friend.setPhoneNo(phone);
						} else if (count == 1) {
							friend.setPhoneNo(phone);
						}
						friend.getPhoneNumbers().add(phone);

					} while (pCur.moveToNext());
				}
				pCur.close();
			}
		}
	}

	private void getEmailDetails(ContentResolver cr, String id, FriendsData friend) {
		Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
		                           ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{id}, null);
		if (emailCur.moveToFirst()) {
			do {
				String email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
				String emailType = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));

				Log.w("Contacts", "Contacts Module = email = " + email + " email type = " + emailType);

				friend.getEmails().add(email);

			} while (emailCur.moveToNext());
		}
	}


	private void getDatesDetails(ContentResolver cr, String id, FriendsData friend) {
		String eventWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
		String[] eventParams = new String[]{id, ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE};
		Cursor birthdayCur = cr.query(ContactsContract.Data.CONTENT_URI, null, eventWhere, eventParams, null);

		if (birthdayCur != null && birthdayCur.moveToFirst()) {
			do {
				String date = birthdayCur.getString(birthdayCur.getColumnIndex(ContactsContract.CommonDataKinds.Event.DATA));
				String type = birthdayCur.getString(birthdayCur.getColumnIndex(ContactsContract.CommonDataKinds.Event.TYPE));

				if (type.equalsIgnoreCase("" + ContactsContract.CommonDataKinds.Event.TYPE_ANNIVERSARY)) {
					Log.v("Contacts", "Anniversary DATES : " + date);
					try {
						friend.setAnniversary(ConstantMethods.getDateFromString(date, false));
					} catch (ParseException e) {
						Log.e("Contacts", " DATES : Exception in parsing the date" + e);
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else if (type.equalsIgnoreCase("" + ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY)) {
					Log.v("Contacts", "Birthday DATES : " + date);
					try {
						friend.setBirthDate(ConstantMethods.getDateFromString(date, false));
					} catch (ParseException e) {
						Log.e("Contacts", " DATES : Exception in parsing the date" + e);
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			} while (birthdayCur.moveToNext());
		}
		if (birthdayCur != null) {
			birthdayCur.close();
		}
	}

	private boolean addToDatabase(FriendsData friend) {
		if (dbHelper == null) {
			dbHelper = new DatabaseHelper(this);
			dbHelper.open();
		}
		return dbHelper.insert(DBUtils.CONTACTS_TABLE_NAME, friend) != -1;
	}

	void openPhoto(ContentResolver cr, Cursor cursor, String id, FriendsData friend) {

		int mThumbnailColumn;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mThumbnailColumn = cursor.getColumnIndex(Contacts.PHOTO_THUMBNAIL_URI);
		} else {
			mThumbnailColumn = cursor.getColumnIndex(Contacts._ID);
		}
		String mThumbnailUri = cursor.getString(mThumbnailColumn);
		Log.d("Image","Thumbnail URI"+mThumbnailUri);
		if (mThumbnailUri != null) {
			Uri thumbUri;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				// Sets the URI from the incoming PHOTO_THUMBNAIL_URI
				thumbUri = Uri.parse(mThumbnailUri);
				friend.setImageUri(thumbUri.toString());
			} else {
				// Prior to Android 3.0, constructs a photo Uri using _ID
				/*
				 * Creates a contact URI from the Contacts content URI incoming
				 * photoData (_ID)
				 */
				final Uri contactUri;
				if (Contacts.CONTENT_URI != null) {
					contactUri = Uri.withAppendedPath(Contacts.CONTENT_URI, mThumbnailUri);
				/*
				 * Creates a photo URI by appending the content URI of
				 * Contacts.Photo.
				 */
					thumbUri = Uri.withAppendedPath(contactUri, Photo.CONTENT_DIRECTORY);
					friend.setImageUri(thumbUri.toString());
				}

			}
		}
	}


	private void sendFinishedBroadcast() {
		// TODO Auto-generated method stub
		Intent notifierIntent = new Intent(Constants.ACTION_CONTACTS_LOADED);
		LocalBroadcastManager.getInstance(this).sendBroadcast(notifierIntent);
	}


}
