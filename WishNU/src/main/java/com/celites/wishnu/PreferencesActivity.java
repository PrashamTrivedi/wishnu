/**
 * 
 */
package com.celites.wishnu;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import com.celites.wishnu.utils.ConstantMethods;
import com.celites.wishnu.utils.Constants;

/**
 * @author Prasham
 * 
 */
public class PreferencesActivity extends PreferenceActivity implements
		OnSharedPreferenceChangeListener {

	private SharedPreferences prefs;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	//	setTheme(R.style.ActionBar_Solid_Purpletheme);
		addPreferencesFromResource(R.xml.preferences);
		ConstantMethods.showPreferences(this);

		prefs = PreferenceManager.getDefaultSharedPreferences(this);

		prefs.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if (key.equals(getString(R.string.sorting_options))) {

			String prefValue = this.prefs.getString(key, "");

			Constants.SORTING_METHOD = Constants.SORT_BY_BIRTHDATE;

			if (prefValue.equals(getString(R.string.sort_by_birthdays))) {
				Constants.SORTING_METHOD = Constants.SORT_BY_BIRTHDATE;
			} else if (prefValue.equals(getString(R.string.sort_by_name))) {
				Constants.SORTING_METHOD = Constants.SORT_BY_NAME;
			}

		} else if (key.equals(getString(R.string.add_alarm_options))) {
			String prefValue = this.prefs.getString(key, "");

			Constants.ALARM_OPTIONS = Constants.SET_ALARM;

			if (prefValue.equals(getString(R.string.set_alarm))) {
				Constants.ALARM_OPTIONS = Constants.SET_ALARM;
			} else if (prefValue.equals(getString(R.string.add_to_calendar))) {
				Constants.ALARM_OPTIONS = Constants.ADD_TO_CALENDAR;
			} else if (prefValue.equals(getString(R.string.both))) {
				Constants.ALARM_OPTIONS = Constants.BOTH;
			}

		}
	}

}
