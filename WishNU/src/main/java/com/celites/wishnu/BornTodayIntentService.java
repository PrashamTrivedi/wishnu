package com.celites.wishnu;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.celites.wishnu.utils.AppExternalFileWriter;
import com.celites.wishnu.utils.AppExternalFileWriter.ExternalFileWriterException;
import com.celites.wishnu.utils.ConstantMethods;
import com.celites.wishnu.utils.Constants;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.loopj.android.http.AsyncHttpClient;
import java.io.File;
import java.io.IOException;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static helper methods.
 */
public class BornTodayIntentService
		extends IntentService {

	public BornTodayIntentService() {
		super("BornTodayIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent != null) {
			long startTime = System.currentTimeMillis();
			String apiKey = "AIzaSyDsGwhmk90FMl5mXcSkZWNqNkqfd_6Hy3s";
			String url = "https://www.googleapis.com/freebase/v1/search";

			AsyncHttpClient httpClient = new AsyncHttpClient();
			Log.d("Url", "Url " + url);
			GenericUrl freebaseUrl = new GenericUrl(url);
			String value = "(all type:/common/topic /people/person/date_of_birth:" + ConstantMethods.getCurrentDateForFreebase() + ")";
			Log.d("Url", "filter " + value);
			freebaseUrl.put("filter", value);
			freebaseUrl.put("limit", "100");
			freebaseUrl.put("key", apiKey);
			freebaseUrl.put("output", "(description:wikipedia /people/person/date_of_birth)");
			try {
				HttpRequest request = new NetHttpTransport().createRequestFactory().buildGetRequest(freebaseUrl);
				HttpResponse httpResponse = request.execute();

				String responseStr = httpResponse.parseAsString();
				AppExternalFileWriter writer = new AppExternalFileWriter(this);
				File bornTodays = writer.createSubDirectory("BornToday", true);
				writer.writeDataToFile(bornTodays, "BornToday", responseStr);
				sendFinishedBroadcast();

			} catch (IOException | ExternalFileWriterException e) {
				e.printStackTrace();
			}
			sendFinishedBroadcast();
		}
	}


	private void sendFinishedBroadcast() {
		Intent notifierIntent = new Intent(Constants.ACTION_BORN_TODAY_LOADED);
		LocalBroadcastManager.getInstance(this).sendBroadcast(notifierIntent);
	}

}
