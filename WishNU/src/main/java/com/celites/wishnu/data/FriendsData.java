/**
 *
 */
package com.celites.wishnu.data;

import android.os.Parcel;
import android.text.format.DateUtils;
import com.celites.wishnu.utils.ConstantMethods;
import com.google.android.gms.plus.model.people.Person;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author prash
 */
public class FriendsData
		implements Comparable<FriendsData>, android.os.Parcelable {

	private long nextDate;

	public FriendsData() {
		emails = new ArrayList<String>();
		phoneNumbers = new ArrayList<String>();
	}

	private String initials;

	/**
	 * Contact id
	 */
	private String contactID;

	/**
	 * facebook id of the contact
	 */
	private String facebookId;

	/**
	 * Google Plus Id Of Contact
	 */
	private String GooglePlusId;

	/**
	 * Contact type of the contact, Either Facebook,Google Plus or local contacts, other may be added later
	 */
	private ContactType type;

	/**
	 * Friends name
	 */
	private String name;


	/**
	 * Friends email id
	 */
	private String emailId;

	/**
	 * Friends phone no
	 */
	private String phoneNo;

	/**
	 * Friends birthdate
	 */
	private long birthDate;
	/**
	 * Image of that friend
	 */
	private String imageUri;

	/**
	 * Is the friend in contact list
	 */
	private boolean isInFriendsList;

	/**
	 * Has user specified Birth year
	 */
	private boolean isWithoutYear;

	/**
	 * Friend's anniversary
	 */
	private long anniversary;

	/**
	 * Collection of phoneNumbers
	 */
	private ArrayList<String> phoneNumbers;
	/**
	 * Collection of emails
	 */
	private ArrayList<String> emails;

	private boolean shouldNotify;
	private boolean shouldNotifyAnniversary;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public boolean isShouldNotify() {
		return shouldNotify;
	}

	public boolean isShouldNotifyAnniversary() {
		return shouldNotifyAnniversary;
	}

	/**
	 * @param name
	 * 		the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 * 		the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the phoneNo
	 */
	public String getPhoneNo() {
		return phoneNo;
	}

	/**
	 * @param phoneNo
	 * 		the phoneNo to set
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * @return the birthDate
	 */
	public long getBirthDate() {
		return birthDate;
	}

	/**
	 * @param birthDate
	 * 		the birthDate to set
	 */
	public void setBirthDate(long birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @param isInFriendsList
	 * 		the isInFriendsList to set
	 */
	public void setInFriendsList(boolean isInFriendsList) {
		this.isInFriendsList = isInFriendsList;
	}

	/**
	 * @return the isInFriendsList
	 */
	public boolean isInFriendsList() {
		return isInFriendsList;
	}

	/**
	 * @return the isWithoutYear
	 */
	public boolean isWithoutYear() {
		return isWithoutYear;
	}

	public void setShouldNotify(boolean shouldNotify) {
		this.shouldNotify = shouldNotify;
	}

	public void setShouldNotifyAnniversary(boolean shouldNotifyAnniversary) {
		this.shouldNotifyAnniversary = shouldNotifyAnniversary;
	}

	/**
	 * @param isWithoutYear
	 * 		the isWithoutYear to set
	 */
	public void setWithoutYear(boolean isWithoutYear) {
		this.isWithoutYear = isWithoutYear;
	}

	/**
	 * @return the image
	 */
	public String getImageUri() {
		return imageUri;
	}

	/**
	 * @param imageUri
	 * 		the image to set
	 */
	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return contactID;
	}

	/**
	 * @param id
	 * 		the id to set
	 */
	public void setId(String id) {
		this.contactID = id;
	}

	private long getNextDate() {
		return nextDate;
	}

	public int compareTo(FriendsData another) {

		if (this.getNextDate() <= 0) {
			// If this.birthdate is not set...
			if (another.getNextDate() <= 0) {
				// And another.birthdate is also not set, treat both as
				// equal, thus compare both contacts by name.
				return compareName(another);
			} else {
				// And another.birthdate is set than return
				// another.birthdate is greater
				return 1;
			}
		} else if (another.getNextDate() <= 0) {
			// If this.birthdate is set but another.birthdate is not set
			// then return this.birthdate as greater
			return -1;
		} else {
			// If both are set, compare properly...
			Date thisDate = new Date(this.getNextDate());
			Date birthDateThis = ConstantMethods.getUpcomingBirthday(thisDate);

			Date anotherDate = new Date(another.getNextDate());
			Date birthDateAnother = ConstantMethods.getUpcomingBirthday(anotherDate);

			Date yesterday = new Date();

			long time = yesterday.getTime();
			yesterday = new Date(time - (DateUtils.DAY_IN_MILLIS));

			long thisDiff = yesterday.getTime() - birthDateThis.getTime();
			long anotherDiff = yesterday.getTime() - birthDateAnother.getTime();

			if (anotherDiff > thisDiff) {
				return 1;
			} else if (anotherDiff == thisDiff) {
				return compareName(another);
			} else {
				return -1;
			}
		}
	}

	private int compareName(FriendsData another) {
		String thisName = this.getName();
		String otherName = another.getName();
		boolean thisStringEmpty = ConstantMethods.isEmptyString(thisName);
		boolean otherStringEmpty = ConstantMethods.isEmptyString(otherName);
		if (thisStringEmpty) {
			return -1;
		} else if (!otherStringEmpty) {
			return thisName.compareToIgnoreCase(otherName);
		} else {
			return 0;
		}
	}


	/**
	 * @return the anniversary
	 */
	public long getAnniversary() {
		return anniversary;
	}

	/**
	 * @param anniversary
	 * 		the anniversary to set
	 */
	public void setAnniversary(long anniversary) {
		this.anniversary = anniversary;
	}

	public void setNextDate() {
		if (birthDate == 0 && anniversary == 0) {
			this.nextDate = 0;
		} else if (birthDate == 0) {
			this.nextDate = anniversary;
		} else if (anniversary == 0) {
			this.nextDate = birthDate;
		} else {
			Date birthdate = new Date(birthDate);
			long nextBirthdate = ConstantMethods.getUpcomingBirthday(birthdate).getTime();
			Date anniversaryDate = new Date(anniversary);
			long nextAnniversary = ConstantMethods.getUpcomingBirthday(anniversaryDate).getTime();
			if (nextBirthdate > nextAnniversary) {
				this.nextDate = nextAnniversary;
			} else {
				this.nextDate = nextBirthdate;

			}
		}
	}

	/**
	 * @return the contactID
	 */
	public String getContactID() {
		return contactID;
	}

	/**
	 * @param contactID
	 * 		the contactID to set
	 */
	public void setContactID(String contactID) {
		this.contactID = contactID;
	}

	/**
	 * @return the type
	 */
	public ContactType getType() {
		return type;
	}

	/**
	 * @param type
	 * 		the type to set
	 */
	public void setType(ContactType type) {
		this.type = type;
	}

	/**
	 * @return the phoneNumbers
	 */
	public ArrayList<String> getPhoneNumbers() {
		return phoneNumbers;
	}

	/**
	 * @param phoneNumbers
	 * 		the phoneNumbers to set
	 */
	public void setPhoneNumbers(ArrayList<String> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}

	/**
	 * @return the emails
	 */
	public ArrayList<String> getEmails() {
		return emails;
	}

	/**
	 * @param emails
	 * 		the emails to set
	 */
	public void setEmails(ArrayList<String> emails) {
		this.emails = emails;
	}


	public static FriendsData createFrom(Person person) {

		FriendsData friend = new FriendsData();

		//		AppLogger.v("Google Pls", "Person Buffer");

		friend.setId(person.getId());

		String displayName = person.getDisplayName();
		friend.setName(displayName);

		String initials = ConstantMethods.prepareInitials(displayName);

		friend.setInitials(initials);


		try {
			if (person.getBirthday() != null) {
				friend.setBirthDate(ConstantMethods.getDateFromString(person.getBirthday(), false));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		friend.setImageUri(person.getImage().getUrl());
		return friend;
	}


	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getGooglePlusId() {
		return GooglePlusId;
	}

	public void setGooglePlusId(String googlePlusId) {
		GooglePlusId = googlePlusId;
	}

	@Override
	public String toString() {
		return getName();
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.initials);
		dest.writeString(this.contactID);
		dest.writeString(this.facebookId);
		dest.writeString(this.GooglePlusId);
		dest.writeInt(this.type == null ? -1 : this.type.ordinal());
		dest.writeString(this.name);
		dest.writeString(this.emailId);
		dest.writeString(this.phoneNo);
		dest.writeLong(this.birthDate);
		dest.writeString(this.imageUri);
		dest.writeByte(isInFriendsList ? (byte) 1 : (byte) 0);
		dest.writeByte(isWithoutYear ? (byte) 1 : (byte) 0);
		dest.writeLong(this.anniversary);
		dest.writeSerializable(this.phoneNumbers);
		dest.writeSerializable(this.emails);
		dest.writeByte(shouldNotify ? (byte) 1 : (byte) 0);
		dest.writeByte(shouldNotifyAnniversary ? (byte) 1 : (byte) 0);
	}

	private FriendsData(Parcel in) {
		this.initials = in.readString();
		this.contactID = in.readString();
		this.facebookId = in.readString();
		this.GooglePlusId = in.readString();
		int tmpType = in.readInt();
		this.type = tmpType == -1 ? null : ContactType.values()[tmpType];
		this.name = in.readString();
		this.emailId = in.readString();
		this.phoneNo = in.readString();
		this.birthDate = in.readLong();
		this.imageUri = in.readString();
		this.isInFriendsList = in.readByte() != 0;
		this.isWithoutYear = in.readByte() != 0;
		this.anniversary = in.readLong();
		this.phoneNumbers = (ArrayList<String>) in.readSerializable();
		this.emails = (ArrayList<String>) in.readSerializable();
		this.shouldNotify = in.readByte() != 0;
		this.shouldNotifyAnniversary = in.readByte() != 0;
	}

	public static final Creator<FriendsData> CREATOR = new Creator<FriendsData>() {
		public FriendsData createFromParcel(Parcel source) {
			return new FriendsData(source);
		}

		public FriendsData[] newArray(int size) {
			return new FriendsData[size];
		}
	};
}
