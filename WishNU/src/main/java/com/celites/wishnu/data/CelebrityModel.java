package com.celites.wishnu.data;

import android.os.Parcel;

/**
 * Created by Prasham on 25-08-2014.
 */
public class CelebrityModel
		implements android.os.Parcelable {

	String id;
	String name;
	String knownFor;

	String description;
	String socialMediaPresence;


	String birthDate;


	public CelebrityModel() {
	}

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKnownFor() {
		return knownFor;
	}

	public void setKnownFor(String knownFor) {
		this.knownFor = knownFor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSocialMediaPresence() {
		return socialMediaPresence;
	}

	public void setSocialMediaPresence(String socialMediaPresence) {
		this.socialMediaPresence = socialMediaPresence;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.id);
		dest.writeString(this.name);
		dest.writeString(this.knownFor);
		dest.writeString(this.description);
		dest.writeString(this.socialMediaPresence);
		dest.writeString(this.birthDate);
	}

	private CelebrityModel(Parcel in) {
		this.id = in.readString();
		this.name = in.readString();
		this.knownFor = in.readString();
		this.description = in.readString();
		this.socialMediaPresence = in.readString();
		this.birthDate = in.readString();
	}

	public static final Creator<CelebrityModel> CREATOR = new Creator<CelebrityModel>() {
		public CelebrityModel createFromParcel(Parcel source) {
			return new CelebrityModel(source);
		}

		public CelebrityModel[] newArray(int size) {
			return new CelebrityModel[size];
		}
	};
}
