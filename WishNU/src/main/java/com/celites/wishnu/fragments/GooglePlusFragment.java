/**
 *
 */
package com.celites.wishnu.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import com.celites.wishnu.BaseActivity;
import com.celites.wishnu.R;
import com.celites.wishnu.adapters.FriendsAdapter;
import com.celites.wishnu.data.ContactType;
import com.celites.wishnu.data.FriendsData;
import com.celites.wishnu.datautils.DBUtils;
import com.celites.wishnu.datautils.DatabaseHelper;
import com.celites.wishnu.utils.AnalyticsLogger;
import com.celites.wishnu.utils.AppLogger;
import com.celites.wishnu.utils.ConstantMethods;
import com.celites.wishnu.utils.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

/**
 * @author Prasham
 */
@SuppressLint("InlinedApi")
public class GooglePlusFragment
		extends RecyclerViewFragment
		implements OnRefreshListener {

	public static final String TAG = "wishNU.GooglePlusFragment";
	private DatabaseHelper dbHelper;
	private BaseActivity baseActivity;
	private GoogleApiClient googleApiClient;
	private FriendsAdapter adapter;
	private ArrayList<FriendsData> friends;

	private BroadcastReceiver googleContactsLoadedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			swipeRefreshLayout.setRefreshing(false);
			loadFriends();

		}
	};
	private int color;
	private SwipeRefreshLayout swipeRefreshLayout;


	public static GooglePlusFragment newInstance() {
		GooglePlusFragment fragment = new GooglePlusFragment();
		return fragment;
	}

	@Override
	public void onRefresh() {
		googleApiClient.connect();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		ActionBar actionBar = baseActivity.getSupportActionBar();
		actionBar.setSubtitle(R.string.menu_google_plus);
		color = getResources().getColor(R.color.google_plus_red);
		actionBar.setBackgroundDrawable(new ColorDrawable(color));
		actionBar.setStackedBackgroundDrawable(new ColorDrawable(color));

		swipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeToRefresh);
		swipeRefreshLayout.setColorSchemeResources(R.color.google_plus_red, R.color.facebook_blue, R.color.app_purple);
		swipeRefreshLayout.setOnRefreshListener(this);
		RecyclerView recyclerView = getRecyclerView();
		recyclerView.setOnScrollListener(new OnScrollListener() {
			/**
			 * Callback method to be invoked when RecyclerView's scroll state changes.
			 *
			 * @param recyclerView
			 * 		The RecyclerView whose scroll state has changed.
			 * @param newState
			 * 		The updated scroll state. One of {@link #SCROLL_STATE_IDLE},  or .
			 */
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
			}

			/**
			 * Callback method to be invoked when the RecyclerView has been scrolled. This will be called after the scroll has
			 * completed.
			 *
			 * @param recyclerView
			 * 		The RecyclerView which scrolled.
			 * @param dx
			 * 		The amount of horizontal scroll.
			 * @param dy
			 * 		The amount of vertical scroll.
			 */
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);
				LayoutManager manager = recyclerView.getLayoutManager();
				if (manager.getClass().equals(LinearLayoutManager.class) || manager.getClass().equals(GridLayoutManager.class)) {
					int firstVisibleItem = ((LinearLayoutManager) manager).findFirstVisibleItemPosition();
					swipeRefreshLayout.setEnabled(firstVisibleItem == 0);
				}

			}
		});

		loadFriends();

	}

	private void loadFriends() {
		friends = getFromDatabase();
		if (ConstantMethods.isArrayListEmpty(friends)) {
			String message = String.format(Locale.getDefault(), getString(R.string.no_contacts_found_message), getString(R.string.menu_google_plus));
			SpannableString ss = ConstantMethods.getEmptyMessage(getResources(), message, color);
			setEmptyText(ss);
//			setAdapter(null);
		} else {
			adapter = new FriendsAdapter(baseActivity, friends, color, ContactType.GOOGLE);
			setAdapter(adapter);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		baseActivity.getAnalyticsLogger().sendScreen("Google+");
	}

	private ArrayList<FriendsData> getFromDatabase() {

		if (dbHelper == null) {
			dbHelper = new DatabaseHelper(baseActivity);
			dbHelper.open();
		}

		ArrayList<FriendsData> friends = null;
		try {
			friends = dbHelper.getAllFriends(DBUtils.GOOGLE_TABLE_NAME);
			AppLogger.d("Google Pls", "Person Buffer Friends Count = " + friends.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (friends != null) {
			Collections.sort(friends, new Comparator<FriendsData>() {
				@Override
				public int compare(FriendsData lhs, FriendsData rhs) {

					return lhs.compareTo(rhs);
				}
			});
		}

		return friends;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		LocalBroadcastManager.getInstance(getActivity())
		                     .registerReceiver(googleContactsLoadedReceiver, new IntentFilter(Constants.ACTION_GOOGLE_LOADED));
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.list_fragment_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

			case R.id.search:
				SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
				searchView.setOnQueryTextListener(new OnQueryTextListener() {
					@Override
					public boolean onQueryTextSubmit(String s) {
						search(s);
						return true;
					}

					@Override
					public boolean onQueryTextChange(String s) {
						search(s);
						return true;
					}
				});
				break;
			default:
				break;
		}
		return true;
	}

	private void search(String queryString) {
		if (adapter != null) {
			adapter.getFilter().filter(queryString);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		baseActivity = (BaseActivity) activity;
		AnalyticsLogger logger = baseActivity.getAnalyticsLogger();
		int servicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(baseActivity);
		String availibility = "Working";
		if (servicesAvailable != ConnectionResult.SUCCESS) {
			availibility = "NotAvailable";
			GooglePlayServicesUtil.getErrorDialog(servicesAvailable, baseActivity, 295).show();
		}
		logger.sendEvent("GooglePlay", "Check Availibility", availibility);

		googleApiClient = baseActivity.getGoogleApiClient();

	}


}
