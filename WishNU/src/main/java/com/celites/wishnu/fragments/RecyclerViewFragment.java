package com.celites.wishnu.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Trying to copy ListFragment functionality with recyclerView. Created by Prasham on 16-12-2014.
 */
public class RecyclerViewFragment
		extends Fragment {
	static final int INTERNAL_EMPTY_ID = 0x00ff0001;
	static final int INTERNAL_EMPTY_CONTAINER_ID = 0x00ff0004;
	static final int INTERNAL_PROGRESS_CONTAINER_ID = 0x00ff0002;
	static final int INTERNAL_LIST_CONTAINER_ID = 0x00ff0003;

	Adapter mAdapter;
	RecyclerView mRecyclerView;
	View mEmptyView;
	TextView mStandardEmptyView;
	View mProgressContainer;
	View mListContainer;
	CharSequence mEmptyText;
	boolean mListShown;
	private LayoutManager mLayoutManager;

	private View mEmptyContainer;

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		final Context context = getActivity();

		FrameLayout root = new FrameLayout(context);

		// ------------------------------------------------------------------

		LinearLayout pframe = new LinearLayout(context);
		pframe.setId(INTERNAL_PROGRESS_CONTAINER_ID);
		pframe.setOrientation(LinearLayout.VERTICAL);
		pframe.setVisibility(View.GONE);
		pframe.setGravity(Gravity.CENTER);

		ProgressBar progress = new ProgressBar(context, null, android.R.attr.progressBarStyleLarge);
		pframe.addView(progress, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

		root.addView(pframe, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

		LinearLayout eframe = new LinearLayout(context);
		eframe.setId(INTERNAL_EMPTY_CONTAINER_ID);
		TextView tv = new TextView(getActivity());
		tv.setId(INTERNAL_EMPTY_ID);
		tv.setGravity(Gravity.CENTER);
		eframe.addView(tv, new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		root.addView(eframe, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

		// ------------------------------------------------------------------

		FrameLayout lframe = new FrameLayout(context);
		lframe.setId(INTERNAL_LIST_CONTAINER_ID);


		RecyclerView recyclerView = new RecyclerView(getActivity());
		recyclerView.setId(android.R.id.list);
		recyclerView.setHasFixedSize(true);

		mLayoutManager = new LinearLayoutManager(getActivity());
		recyclerView.setLayoutManager(mLayoutManager);
		//		lv.setDrawSelectorOnTop(false);
		lframe.addView(recyclerView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

		root.addView(lframe, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

		// ------------------------------------------------------------------

		root.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

		return root;
	}

	public void setmLayoutManager(LayoutManager mLayoutManager) {
		this.mLayoutManager = mLayoutManager;
	}

	/**
	 * Provide the cursor for the list view.
	 */
	public void setAdapter(Adapter adapter) {
		boolean hadAdapter = mAdapter != null;
		mAdapter = adapter;
		if (mRecyclerView != null) {
			mRecyclerView.setAdapter(adapter);
			if (!mListShown && !hadAdapter) {
				// The list was hidden, and previously didn't have an
				// adapter.  It is now time to show it.
				setListShown(true, getView().getWindowToken() != null);
			}
		}
	}

	/**
	 * Control whether the list is being displayed.  You can make it not displayed if you are waiting for the initial data to show in it.  During this
	 * time an indeterminant progress indicator will be shown instead.
	 *
	 * @param shown
	 * 		If true, the list view is shown; if false, the progress indicator.  The initial value is true.
	 * @param animate
	 * 		If true, an animation will be used to transition to the new state.
	 */
	private void setListShown(boolean shown, boolean animate) {
		ensureList();
		if (mProgressContainer == null) {
			throw new IllegalStateException("Can't be used with a custom content view");
		}
		if (mListShown == shown) {
			return;
		}
		mListShown = shown;
		if (shown) {
			if (animate) {
				mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out));
				mEmptyContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out));
				mListContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
			} else {
				mProgressContainer.clearAnimation();
				mEmptyContainer.clearAnimation();
				mListContainer.clearAnimation();
			}
			mProgressContainer.setVisibility(View.GONE);
			mEmptyContainer.setVisibility(View.GONE);
			mListContainer.setVisibility(View.VISIBLE);
		} else {
			if (animate) {
				mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
				mEmptyContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
				mListContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out));
			} else {
				mProgressContainer.clearAnimation();
				mEmptyContainer.clearAnimation();
				mListContainer.clearAnimation();
			}
			mProgressContainer.setVisibility(View.VISIBLE);
			mEmptyContainer.setVisibility(View.VISIBLE);
			mListContainer.setVisibility(View.GONE);
		}
	}

	public void setEmptyView(View view) {
		if (view.getParent() == null) {
			ViewGroup container = (ViewGroup) mProgressContainer;
			container.removeAllViews();
			container.addView(view);
		}
		mProgressContainer.setVisibility(View.VISIBLE);
		mListContainer.setVisibility(View.GONE);
		mEmptyContainer.setVisibility(View.GONE);
	}

	private void ensureList() {
		if (mRecyclerView != null) {
			return;
		}
		View root = getView();
		if (root == null) {
			throw new IllegalStateException("Content view not yet created");
		}
		if (root instanceof RecyclerView) {
			mRecyclerView = (RecyclerView) root;
		} else {
			mStandardEmptyView = (TextView) root.findViewById(INTERNAL_EMPTY_ID);
			if (mStandardEmptyView == null) {
				mEmptyView = root.findViewById(android.R.id.empty);
			} else {
				mStandardEmptyView.setVisibility(View.GONE);
			}
			mProgressContainer = root.findViewById(INTERNAL_PROGRESS_CONTAINER_ID);
			mEmptyContainer = root.findViewById(INTERNAL_EMPTY_CONTAINER_ID);
			mListContainer = root.findViewById(INTERNAL_LIST_CONTAINER_ID);
			View rawListView = root.findViewById(android.R.id.list);
			if (!(rawListView instanceof RecyclerView)) {
				if (rawListView == null) {
					throw new RuntimeException("Your content must have a RecyclerView whose id attribute is " + "'android.R.id.list'");
				}
				throw new RuntimeException("Content has view with id attribute 'android.R.id.list' " + "that is not a RecyclerView class");
			}
			mRecyclerView = (RecyclerView) rawListView;
			if (mEmptyView != null) {
				setEmptyView(mEmptyView);
			} else if (mEmptyText != null) {
				mStandardEmptyView.setText(mEmptyText);
				setEmptyView(mStandardEmptyView);
			}
		}
		mListShown = true;

		//		mRecyclerView.setOnItemClickListener(mOnClickListener);
		if (mAdapter != null) {
			Adapter adapter = mAdapter;
			mAdapter = null;
			setAdapter(adapter);
		} else {
			// We are starting without an adapter, so assume we won't
			// have our data right away and start with the progress indicator.
			if (mProgressContainer != null) {
				setListShown(false, false);
			}
		}
		//		mHandler.post(mRequestFocus);
	}

	/**
	 * Attach to list view once the view hierarchy has been created.
	 */
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		ensureList();
	}

	public RecyclerView getRecyclerView() {
		return mRecyclerView;
	}

	/**
	 * The default content for a ListFragment has a TextView that can be shown when the list is empty.  If you would like to have it shown, call this
	 * method to supply the text it should use.
	 */
	public void setEmptyText(CharSequence text) {
		ensureList();
		if (mStandardEmptyView == null) {
			throw new IllegalStateException("Can't be used with a custom content view");
		}
		if (mEmptyText == null) {
			mProgressContainer.setVisibility(View.GONE);
			mListContainer.setVisibility(View.GONE);
			mEmptyContainer.setVisibility(View.VISIBLE);
			mStandardEmptyView.setVisibility(View.VISIBLE);
		}
		mStandardEmptyView.setText(text);
		mEmptyText = text;
	}

	public Adapter getAdapter() {
		return mAdapter;
	}

	/**
	 * Control whether the list is being displayed.  You can make it not displayed if you are waiting for the initial data to show in it.  During this
	 * time an indeterminant progress indicator will be shown instead.
	 * <p/>
	 * <p>Applications do not normally need to use this themselves.  The default behavior of ListFragment is to start with the list not being shown,
	 * only showing it once an adapter is given with {@link #setListAdapter(android.widget.ListAdapter)}. If the list at that point had not been
	 * shown, when it does get shown it will be do without the user ever seeing the hidden state.
	 *
	 * @param shown
	 * 		If true, the list view is shown; if false, the progress indicator.  The initial value is true.
	 */
	public void setListShown(boolean shown) {
		setListShown(shown, true);
	}


	/**
	 * Like {@link #setListShown(boolean)}, but no animation is used when transitioning from the previous state.
	 */
	public void setListShownNoAnimation(boolean shown) {
		setListShown(shown, false);
	}

	/**
	 * Detach from list view.
	 */
	@Override
	public void onDestroyView() {
		//		mHandler.removeCallbacks(mRequestFocus);
		mRecyclerView = null;
		mListShown = false;
		mEmptyView = null;
		mProgressContainer = null;
		mListContainer = null;
		mStandardEmptyView = null;
		super.onDestroyView();
	}
}
