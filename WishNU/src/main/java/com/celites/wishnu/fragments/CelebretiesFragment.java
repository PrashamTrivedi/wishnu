package com.celites.wishnu.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import com.celites.wishnu.BaseActivity;
import com.celites.wishnu.BornTodayIntentService;
import com.celites.wishnu.R;
import com.celites.wishnu.adapters.BornTodayAdapter;
import com.celites.wishnu.data.CelebrityModel;
import com.celites.wishnu.utils.AppExternalFileWriter;
import com.celites.wishnu.utils.AppExternalFileWriter.ExternalFileWriterException;
import com.celites.wishnu.utils.ConstantMethods;
import com.celites.wishnu.utils.Constants;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Prasham on 22-08-2014.
 */
public class CelebretiesFragment
		extends RecyclerViewFragment
		implements OnRefreshListener {

	public static final String TAG = "wishNu.CelebretiesFragment";
	private ArrayList<CelebrityModel> models;
	private BornTodayAdapter adapter;
	private BaseActivity baseActivity;
	private int color;
	private BroadcastReceiver celebritiesLoadedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			swipeRefreshLayout.setRefreshing(false);
			loadBornToday();
		}
	};
	private SwipeRefreshLayout swipeRefreshLayout;


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		baseActivity = (BaseActivity) getActivity();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		LocalBroadcastManager.getInstance(getActivity())
		                     .registerReceiver(celebritiesLoadedReceiver, new IntentFilter(Constants.ACTION_BORN_TODAY_LOADED));
	}

	/**
	 * Instantiates new fragment.
	 *
	 * @param bundle
	 * 		: Any extra data to be passed to fragment, pass null if you don't want to pass anything.
	 *
	 * @return : Instance of current fragment.
	 */
	public static CelebretiesFragment newInstance(Bundle bundle) {
		CelebretiesFragment celebratiesFragment = new CelebretiesFragment();
		if (bundle != null) {
			celebratiesFragment.setArguments(bundle);
		}
		return celebratiesFragment;
	}

	@Override
	public void onRefresh() {
		Intent contactServiceIntent = new Intent(getActivity(), BornTodayIntentService.class);
		baseActivity.startService(contactServiceIntent);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		ActionBar actionBar = baseActivity.getSupportActionBar();
		actionBar.setSubtitle(R.string.menu_born_today);
		color = getResources().getColor(R.color.app_purple);
		actionBar.setBackgroundDrawable(new ColorDrawable(color));
		actionBar.setStackedBackgroundDrawable(new ColorDrawable(color));

		swipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeToRefresh);
		swipeRefreshLayout.setColorSchemeResources(R.color.app_purple, R.color.facebook_blue, R.color.google_plus_red);
		swipeRefreshLayout.setOnRefreshListener(this);

		RecyclerView recyclerView = getRecyclerView();
		recyclerView.setOnScrollListener(new OnScrollListener() {
			/**
			 * Callback method to be invoked when RecyclerView's scroll state changes.
			 *
			 * @param recyclerView
			 * 		The RecyclerView whose scroll state has changed.
			 * @param newState
			 * 		The updated scroll state. One of ,  or .
			 */
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
			}

			/**
			 * Callback method to be invoked when the RecyclerView has been scrolled. This will be called after the scroll has completed.
			 *
			 * @param recyclerView
			 * 		The RecyclerView which scrolled.
			 * @param dx
			 * 		The amount of horizontal scroll.
			 * @param dy
			 * 		The amount of vertical scroll.
			 */
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);
				LayoutManager manager = recyclerView.getLayoutManager();
				if (manager.getClass().equals(LinearLayoutManager.class) || manager.getClass().equals(GridLayoutManager.class)) {
					int firstVisibleItem = ((LinearLayoutManager) manager).findFirstVisibleItemPosition();
					swipeRefreshLayout.setEnabled(firstVisibleItem == 0);
				}

			}
		});

		loadBornToday();

	}

	private void loadBornToday() {
		models = getFromFile();
		if (ConstantMethods.isArrayListEmpty(models)) {
			String message = String.format(Locale.getDefault(), getString(R.string.no_contacts_found_message), getString(R.string.menu_born_today));
			SpannableString ss = ConstantMethods.getEmptyMessage(getResources(), message, color);
			setEmptyText(ss);
			//			setAdapter(null);
		} else {
			adapter = new BornTodayAdapter(baseActivity);
			adapter.setModels(models);
			setAdapter(adapter);
		}
	}

	private ArrayList<CelebrityModel> getFromFile() {
		ArrayList<CelebrityModel> celebrityModels = new ArrayList<CelebrityModel>();
		AppExternalFileWriter writer = new AppExternalFileWriter(baseActivity);
		try {
			File bornTodays = writer.createSubDirectory("BornToday", true);
			File[] list = bornTodays.listFiles();
			File bornToday = null;
			for (File file : list) {
				if (file.getName().equals("BornToday")) {
					bornToday = file;
				}
			}

			File file = (bornToday == null) ? new File(bornTodays, "BornToday") : bornToday;
			BufferedReader br = new BufferedReader(new FileReader(file));
			StringBuilder text = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				text.append(line);
			}
			if (!ConstantMethods.isEmptyString(text)) {
				celebrityModels = parseJson(text.toString());
			} else {
				onRefresh();
			}
		} catch (ExternalFileWriterException | IOException | JSONException e1) {
			e1.printStackTrace();
		}
		return celebrityModels;
	}

	private ArrayList<CelebrityModel> parseJson(String responseStr) throws JSONException {
		ArrayList<CelebrityModel> celebreties = new ArrayList<CelebrityModel>();
		JSONObject response = new JSONObject(responseStr);
		JSONArray results = response.optJSONArray("result");
		if (results != null) {
			for (int i = 0; i < results.length(); i++) {
				JSONObject celebrityJson = results.optJSONObject(i);
				if (celebrityJson != null) {
					CelebrityModel celebrityModel = new CelebrityModel();
					celebrityModel.setId(celebrityJson.optString("id"));
					String name = celebrityJson.optString("name");
					celebrityModel.setName(name);
					Log.w("Celebrity", "Celebrity Processing: " + name);
					JSONObject notableFor = celebrityJson.optJSONObject("notable");
					if (notableFor != null) {
						celebrityModel.setKnownFor(notableFor.optString("name"));
					}

					JSONObject outPut = celebrityJson.optJSONObject("output");
					if (outPut != null) {
						JSONObject descriptionWikipedia = outPut.optJSONObject("description:wikipedia");
						if (descriptionWikipedia != null) {
							JSONArray descriptionJson = descriptionWikipedia.optJSONArray("/common/topic/description");
							if (descriptionJson != null) {
								StringBuilder descriptionBuilder = new StringBuilder();
								for (int j = 0; j < descriptionJson.length(); j++) {
									descriptionBuilder.append(descriptionJson.optString(j)).append("\n");

								}
								celebrityModel.setDescription(descriptionBuilder.toString());
							}
						}


						JSONObject birthdayJson = outPut.optJSONObject("/people/person/date_of_birth");
						if (birthdayJson != null) {
							JSONArray birthday = birthdayJson.optJSONArray("/people/person/date_of_birth");
							celebrityModel.setBirthDate(birthday.optString(0));
						}
					}
					celebreties.add(celebrityModel);
				}
			}


		}
		return celebreties;
	}

	@Override
	public void onResume() {
		super.onResume();
		baseActivity.getAnalyticsLogger().sendScreen("Born Today");
	}


}


