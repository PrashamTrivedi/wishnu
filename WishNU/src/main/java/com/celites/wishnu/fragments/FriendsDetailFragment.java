package com.celites.wishnu.fragments;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.graphics.Palette;
import android.support.v7.graphics.Palette.PaletteAsyncListener;
import android.support.v7.graphics.Palette.Swatch;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import com.celites.wishnu.BaseActivity;
import com.celites.wishnu.R;
import com.celites.wishnu.data.ContactType;
import com.celites.wishnu.data.FriendsData;
import com.celites.wishnu.datautils.DBUtils;
import com.celites.wishnu.datautils.DatabaseHelper;
import com.celites.wishnu.interfaces.WishActionListener;
import com.celites.wishnu.utils.AnalyticsLogger;
import com.celites.wishnu.utils.ConstantMethods;
import com.celites.wishnu.utils.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusClient.OnPeopleLoadedListener;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class FriendsDetailFragment
		extends Fragment
		implements OnClickListener, WishActionListener {

	public static final String TAG = "wishNU.FriendsDetail";
	private BaseActivity baseActivity;
	private FriendsData friend;
	private View friendDetailLayout;
	private ImageView friendsImage;
	private TextView nameText;
	private TextView birthDate;
	private Button birthDateButton;
	private Button anniversaryButton;
	private Spinner emailsSpinner;
	private Spinner numbersSpinner;
	private OnClickListener dateSelectiorListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub


			final Button button = (Button) v;
			String date = "" + button.getText();
			Date dateFromString;
			try {
				dateFromString = Constants.FORMAT_SHOW_DATES.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
				dateFromString = new Date();
			} catch (Exception e) {
				e.printStackTrace();
				dateFromString = new Date();
			}

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateFromString);

			OnDateSetListener onDateSetListener = new OnDateSetListener() {

				@Override
				public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

					Calendar dateCalendar = Calendar.getInstance();
					dateCalendar.set(Calendar.YEAR, year);
					dateCalendar.set(Calendar.MONTH, monthOfYear);
					dateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

					String selectedDate = Constants.FORMAT_SHOW_DATES.format(dateCalendar.getTime());

					button.setText(selectedDate);
					analyticsLogger.sendEvent("Input", "Date", "Entered");
				}
			};


			DatePickerDialog datePickerDialog;
			try {
				datePickerDialog = new DatePickerDialog(baseActivity, onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				                                        calendar.get(Calendar.DAY_OF_MONTH));
			} catch (Exception e) {
				Calendar today = Calendar.getInstance();
				datePickerDialog = new DatePickerDialog(baseActivity, onDateSetListener, today.get(Calendar.YEAR), today.get(Calendar.MONTH),
				                                        today.get(Calendar.DAY_OF_MONTH));
			}
			datePickerDialog.show();
		}
	};
	private DatabaseHelper dbHelper;
	private String tableName;
	private PlusClient plusClient;
	private GoogleApiClient googleApiClient;
	private ImageButton wishOnGoogleButton;
	private TextView anniversary;
	private OnMenuItemClickListener callMenuListener = new OnMenuItemClickListener() {
		@Override
		public boolean onMenuItemClick(MenuItem item) {
			int id = item.getItemId();
			if (id == R.id.callNumber) {
				Object selectedItem = numbersSpinner.getSelectedItem();
				String selectedPhoneNumber = selectedItem.toString();
				if (!TextUtils.isEmpty(selectedPhoneNumber)) {
					Intent intent = new Intent(Intent.ACTION_DIAL);
					analyticsLogger.sendEvent("Phone", "CallButton", "Call");
					intent.setData(Uri.parse("tel:" + selectedPhoneNumber));
					startActivity(intent);
					return true;
				}
			} else if (id == R.id.send_sms) {
				Object selectedItem = numbersSpinner.getSelectedItem();
				String selectedPhoneNumber = selectedItem.toString();
				if (!TextUtils.isEmpty(selectedPhoneNumber)) {
					Intent sms = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", selectedPhoneNumber, null));
					if (sms.resolveActivity(getActivity().getPackageManager()) != null) {
						analyticsLogger.sendEvent("Phone", "CallButton", "SMS");
						startActivity(sms);
					}
				}
				return true;
			}
			return false;
		}
	};
	private AnalyticsLogger analyticsLogger;
	private View socialNetworkParent;
	private Button googlePlusButton;


	private void initiateView() {
		friendsImage = (ImageView) friendDetailLayout.findViewById(R.id.contactImage);
		nameText = (TextView) friendDetailLayout.findViewById(R.id.contactName);
		birthDate = (TextView) friendDetailLayout.findViewById(R.id.birthday);
		anniversary = (TextView) friendDetailLayout.findViewById(R.id.anniversary);

		birthDateButton = (Button) friendDetailLayout.findViewById(R.id.birthdateButton);

		birthDateButton.setOnClickListener(dateSelectiorListener);

		anniversaryButton = (Button) friendDetailLayout.findViewById(R.id.anniversaryButton);

		anniversaryButton.setOnClickListener(dateSelectiorListener);

		ImageButton birthDaySetAlarmButton = (ImageButton) friendDetailLayout.findViewById(R.id.birthdayAlarm);
		ImageButton anniversarySetAlarmButton = (ImageButton) friendDetailLayout.findViewById(R.id.anniversaryAlarm);

		birthDaySetAlarmButton.setOnClickListener(this);
		anniversarySetAlarmButton.setOnClickListener(this);


		numbersSpinner = (Spinner) friendDetailLayout.findViewById(R.id.numbersSpinner);
		emailsSpinner = (Spinner) friendDetailLayout.findViewById(R.id.emailsSpinner);

		ImageButton callButton = (ImageButton) friendDetailLayout.findViewById(R.id.callButton);
		callButton.setOnClickListener(this);

		ImageButton emailButton = (ImageButton) friendDetailLayout.findViewById(R.id.mailButton);
		emailButton.setOnClickListener(this);

		googlePlusButton = (Button) friendDetailLayout.findViewById(R.id.googlePlusButton);

		wishOnGoogleButton = (ImageButton) friendDetailLayout.findViewById(R.id.wishOnGPlusButton);
		wishOnGoogleButton.setOnClickListener(this);

		socialNetworkParent = friendDetailLayout.findViewById(R.id.socialNetworkParent);

	}

	private void populateFriendsDetail() {
		ConstantMethods.getPicasso(baseActivity).load(friend.getImageUri()).into(new Target() {
			@Override
			public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
				friendsImage.setImageBitmap(bitmap);
				PaletteAsyncListener listener = new PaletteAsyncListener() {
					@Override
					public void onGenerated(Palette palette) {
						Swatch swatch = palette.getVibrantSwatch();
						if (swatch != null) {
							ActionBar actionBar = baseActivity.getSupportActionBar();
							if (actionBar != null) {
								actionBar.setBackgroundDrawable(new ColorDrawable(swatch.getRgb()));
								actionBar.setStackedBackgroundDrawable(new ColorDrawable(swatch.getRgb()));

							}
						}
					}
				};
				Palette.generateAsync(bitmap, 24, listener);
			}

			@Override
			public void onBitmapFailed(Drawable errorDrawable) {

			}

			@Override
			public void onPrepareLoad(Drawable placeHolderDrawable) {

			}
		});

		String name = friend.getName();
		nameText.setText(name);
		String birthdateText = "";
		String birthdate = "";
		long birthdateData = friend.getBirthDate();
		if (birthdateData > 0) {
			birthdateText = ConstantMethods.getDateStringToShow(birthdateData);
			birthdate = getString(R.string.birthdayHeader, birthdateText);
		}
		birthDateButton.setText(birthdateText);
		birthDate.setText(birthdate);

		String anniversaryText = "";
		String anniversaryString = "";
		long anniversaryData = friend.getAnniversary();
		if (anniversaryData > 0) {

			anniversaryText = ConstantMethods.getDateStringToShow(anniversaryData);
			anniversaryString = getString(R.string.anniversaryHeader, anniversaryText);
		}
		anniversary.setText(anniversaryString);
		anniversaryButton.setText(anniversaryText);

		if (friend.getPhoneNumbers() != null && !friend.getPhoneNumbers().isEmpty()) {
			ArrayAdapter<String> phoneAdapter =
					new ArrayAdapter<String>(baseActivity, android.R.layout.simple_spinner_dropdown_item, friend.getPhoneNumbers());
			numbersSpinner.setAdapter(phoneAdapter);
		}
		if (friend.getEmails() != null && !friend.getEmails().isEmpty()) {
			ArrayAdapter<String> emailsAdapter =
					new ArrayAdapter<String>(baseActivity, android.R.layout.simple_spinner_dropdown_item, friend.getEmails());
			emailsSpinner.setAdapter(emailsAdapter);
		}

		if (TextUtils.isEmpty(friend.getGooglePlusId())) {

			wishOnGoogleButton.setEnabled(false);
		} else {
			googlePlusButton.setText(name);
			socialNetworkParent.setVisibility(View.VISIBLE);
		}
	}


	private void postToGooglePlusStream() {
		// TODO Auto-generated method stub
		Crouton.makeText(baseActivity, "Connecting with Google+, please wait", Style.INFO).show();

		if (!plusClient.isConnected()) {
			plusClient.connect();
		} else {
			wishFriend(plusClient);

		}
	}

	private void wishFriend(PlusClient plusClient) {

		plusClient.loadPeople(new OnPeopleLoadedListener() {
			@Override
			public void onPeopleLoaded(ConnectionResult result, PersonBuffer persons, String s) {
				ArrayList<Person> people = new ArrayList<Person>();
				Person person = persons.get(0);
				people.add(person);
				if (person != null) {
					Intent shareIntent = new PlusShare.Builder(baseActivity).setType("text/plain").setText("Hi :)").setRecipients(people).getIntent();

					startActivityForResult(shareIntent, 0);

				}

			}


		}, friend.getGooglePlusId());
	}

	private void callContactMenu(View v) {
		PopupMenu popup = new PopupMenu(v.getContext(), v);
		popup.setOnMenuItemClickListener(callMenuListener);

		MenuInflater inflater = popup.getMenuInflater();
		inflater.inflate(R.menu.numbermenu, popup.getMenu());
		popup.show();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		friendDetailLayout = inflater.inflate(R.layout.friendsdetail, container, false);

		return friendDetailLayout;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		analyticsLogger = baseActivity.getAnalyticsLogger();
		try {
			friend = this.getArguments().getParcelable(getActivity().getString(R.string.friends_extra));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (friend != null) {
			baseActivity.getSupportActionBar().setSubtitle(friend.getName());

			if (friend.getType() == ContactType.ADDRESS_BOOK) {
				tableName = DBUtils.CONTACTS_TABLE_NAME;
			} else if (friend.getType() == ContactType.FACEBOOK) {
				tableName = DBUtils.FACEBOOK_TABLE_NAME;
				if (TextUtils.isEmpty(friend.getFacebookId())) {
					friend.setFacebookId(friend.getId());
				}
			} else if (friend.getType() == ContactType.GOOGLE) {
				tableName = DBUtils.GOOGLE_TABLE_NAME;
				if (TextUtils.isEmpty(friend.getGooglePlusId())) {
					friend.setGooglePlusId(friend.getId());
				}
			}

			initiateView();
			populateFriendsDetail();

		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		baseActivity = (BaseActivity) getActivity();
		dbHelper = baseActivity.getDbHelper();

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.wish_friend_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();

		if (id == R.id.saveDetails) {
			analyticsLogger.sendEvent("Phone", "MenuAction", "Save");
			try {
				friend.setBirthDate(ConstantMethods.parseDate("" + birthDateButton.getText()));

			} catch (ParseException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				friend.setAnniversary(ConstantMethods.parseDate("" + anniversaryButton.getText()));
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (!TextUtils.isEmpty(tableName)) dbHelper.insert(tableName, friend);
			populateFriendsDetail();
		}


		return super.onOptionsItemSelected(item);
	}


	@Override
	public void wishOnGooglePlus() {
		// TODO Auto-generated method stub

		if (TextUtils.isEmpty(friend.getGooglePlusId())) {
			Crouton.makeText(baseActivity, "Google+ details not available", Style.ALERT).show();
		} else {
			postToGooglePlusStream();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.callButton) {

			callContactMenu(v);
		}
		if (v.getId() == R.id.mailButton) {
			String email = emailsSpinner.getSelectedItem().toString();
			startActivity(ConstantMethods.sendEmailTo(new String[]{email}));
		}
		if (v.getId() == R.id.birthdayAlarm) {
			callAlarmMenu(v);
		}
		if (v.getId() == R.id.anniversaryAlarm) {
			callAlarmMenu(v);
		}

	}

	private void callAlarmMenu(final View v) {
		final boolean isBirthday = v.getId() == R.id.birthdayAlarm;
		PopupMenu popup = new PopupMenu(v.getContext(), v);

		popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				int id = item.getItemId();

				long date = (isBirthday) ? friend.getBirthDate() : friend.getAnniversary();
				if (id == R.id.setAlarm) {
					analyticsLogger.sendEvent("Alarm", "AlarmAction", "Notification");
					ConstantMethods.setAlarm(v.getContext(), friend, isBirthday);
					return true;

				} else if (id == R.id.addToCalendar) {
					String event = (isBirthday) ? "Birthday" : "Anniversary";
					analyticsLogger.sendEvent("Alarm", "AlarmAction", "Calendar");
					ConstantMethods.addDateToCalendar(friend, v.getContext(), date, event);
					return true;
				}
				return false;

			}
		});


		MenuInflater inflater = popup.getMenuInflater();
		inflater.inflate(R.menu.savemenu, popup.getMenu());
		MenuItem setAlarmItem = popup.getMenu().findItem(R.id.setAlarm);


		boolean canShowMenu = false;
		if (isBirthday) {
			if (friend.getBirthDate() > 0) {
				canShowMenu = true;
				int resourceId = (friend.isShouldNotify()) ? R.string.remove_alarm : R.string.set_alarm;
				setAlarmItem.setTitle(resourceId);
			}
		} else if (friend.getAnniversary() > 0) {
			canShowMenu = true;
			int resourceId = (friend.isShouldNotifyAnniversary()) ? R.string.remove_alarm : R.string.set_alarm;
			setAlarmItem.setTitle(resourceId);
		}
		if (canShowMenu) {
			popup.show();
		}
	}


}
