/**
 *
 */
package com.celites.wishnu.fragments;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.celites.wishnu.BaseActivity;
import com.celites.wishnu.ContactFetcherService;
import com.celites.wishnu.R;
import com.celites.wishnu.adapters.FriendsAdapter;
import com.celites.wishnu.data.ContactType;
import com.celites.wishnu.data.FriendsData;
import com.celites.wishnu.datautils.DBUtils;
import com.celites.wishnu.datautils.DatabaseHelper;
import com.celites.wishnu.utils.AnalyticsLogger;
import com.celites.wishnu.utils.ConstantMethods;
import com.celites.wishnu.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

/**
 * @author Prasham
 */
@SuppressLint("InlinedApi")
public class ContactsListFragment
		extends RecyclerViewFragment
		implements OnRefreshListener

{

	public static final String TAG = "wishNu.ContactsFragment";
	private DatabaseHelper dbHelper;
	private BaseActivity baseActivity;
	private ArrayList<FriendsData> friends;
	private FriendsAdapter adapter;
	private BroadcastReceiver contactLoadedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			swipeRefreshLayout.setRefreshing(false);
			loadFriends();

		}
	};
	private int color;
	private AnalyticsLogger analyticsLogger;
	private SwipeRefreshLayout swipeRefreshLayout;


	public static ContactsListFragment newInstance() {
		ContactsListFragment fragment = new ContactsListFragment();


		return fragment;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(contactLoadedReceiver, new IntentFilter(Constants.ACTION_CONTACTS_LOADED));
	}

	@Override
	public void onRefresh() {
		startService();
	}


	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		baseActivity = (BaseActivity) getActivity();
		dbHelper = baseActivity.getDbHelper();

		ActionBar actionBar = baseActivity.getSupportActionBar();
		actionBar.setSubtitle(R.string.menu_contacts);
		color = getResources().getColor(R.color.app_purple);
		actionBar.setBackgroundDrawable(new ColorDrawable(color));
		actionBar.setStackedBackgroundDrawable(new ColorDrawable(color));


		swipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeToRefresh);
		swipeRefreshLayout.setColorSchemeResources(R.color.app_purple, R.color.facebook_blue, R.color.google_plus_red);
		swipeRefreshLayout.setOnRefreshListener(this);

		RecyclerView recyclerView = getRecyclerView();
		recyclerView.setOnScrollListener(new OnScrollListener() {
			/**
			 * Callback method to be invoked when RecyclerView's scroll state changes.
			 *
			 * @param recyclerView
			 * 		The RecyclerView whose scroll state has changed.
			 * @param newState
			 * 		The updated scroll state. One of {@link #SCROLL_STATE_IDLE}, {@link #SCROLL_STATE_DRAGGING} or {@link
			 * 		#SCROLL_STATE_SETTLING}.
			 */
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
			}

			/**
			 * Callback method to be invoked when the RecyclerView has been scrolled. This will be called after the scroll has
			 * completed.
			 *
			 * @param recyclerView
			 * 		The RecyclerView which scrolled.
			 * @param dx
			 * 		The amount of horizontal scroll.
			 * @param dy
			 * 		The amount of vertical scroll.
			 */
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);
				LayoutManager manager = recyclerView.getLayoutManager();
				if (manager.getClass().equals(LinearLayoutManager.class) || manager.getClass().equals(GridLayoutManager.class)) {
					int firstVisibleItem = ((LinearLayoutManager) manager).findFirstVisibleItemPosition();
					swipeRefreshLayout.setEnabled(firstVisibleItem == 0);
				}

			}
		});
		loadFriends();
	}

	private void loadFriends() {
		friends = getFromDatabase();
		if (ConstantMethods.isArrayListEmpty(friends)) {
			String message = String.format(Locale.getDefault(), getString(R.string.no_contacts_found_message), getString(R.string.menu_contacts));
			SpannableString ss = ConstantMethods.getEmptyMessage(getResources(), message, color);
			setEmptyText(ss);
			setAdapter(null);
		} else {
			adapter = new FriendsAdapter(baseActivity, friends, color, ContactType.ADDRESS_BOOK);
			setAdapter(adapter);
		}
	}


	private ArrayList<FriendsData> getFromDatabase() {

		if (dbHelper == null) {
			dbHelper = new DatabaseHelper(baseActivity);
			dbHelper.open();
		}

		ArrayList<FriendsData> friends = null;
		try {
			friends = dbHelper.getAllFriends(DBUtils.CONTACTS_TABLE_NAME);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (friends != null) {
			Collections.sort(friends, new Comparator<FriendsData>() {
				@Override
				public int compare(FriendsData lhs, FriendsData rhs) {

					return lhs.compareTo(rhs);
				}
			});
		}

		return friends;
	}


	@Override
	public void onResume() {
		super.onResume();
		analyticsLogger = baseActivity.getAnalyticsLogger();
		analyticsLogger.sendScreen("Contacts");
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.list_fragment_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

			case R.id.search:
				SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
				searchView.setOnQueryTextListener(new OnQueryTextListener() {
					@Override
					public boolean onQueryTextSubmit(String s) {
						search(s);
						return true;
					}

					@Override
					public boolean onQueryTextChange(String s) {
						search(s);
						return true;
					}
				});
				break;
			default:
				break;
		}
		return true;
	}

	private void startService() {
		Intent contactServiceIntent = new Intent(getActivity(), ContactFetcherService.class);
		baseActivity.startService(contactServiceIntent);
	}

	private void search(String queryString) {
		analyticsLogger.sendEvent("MenuItem", "Search", "Queried");
		if (adapter != null) {
			adapter.getFilter().filter(queryString);
		}
	}


}
