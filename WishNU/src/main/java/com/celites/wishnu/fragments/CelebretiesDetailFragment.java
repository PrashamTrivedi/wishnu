package com.celites.wishnu.fragments;

import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.graphics.Palette;
import android.support.v7.graphics.Palette.PaletteAsyncListener;
import android.support.v7.graphics.Palette.Swatch;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.celites.wishnu.BaseActivity;
import com.celites.wishnu.R;
import com.celites.wishnu.data.CelebrityModel;
import com.celites.wishnu.utils.ConstantMethods;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;

public class CelebretiesDetailFragment
		extends Fragment {

	public static final String TAG = "wishNU.CelebretiesFragment";
	private BaseActivity baseActivity;
	private CelebrityModel celebrityModel;
	private TextView wikipediaDescription;
	private TextView nameText;
	private View friendDetailLayout;
	private ImageView friendsImage;
	private TextView knownFor;

	private void initiateView(View view) {
		friendsImage = (ImageView) view.findViewById(R.id.contactImage);
		nameText = (TextView) view.findViewById(R.id.contactName);
		knownFor = (TextView) view.findViewById(R.id.birthday);
		wikipediaDescription = (TextView) view.findViewById(R.id.wikipediaDescription);
		wikipediaDescription.setMovementMethod(new ScrollingMovementMethod());
		((TextView) view.findViewById(R.id.anniversary)).setText("");
	}

	private void populateFriendsDetail() {
		Picasso picasso = ConstantMethods.getPicasso(baseActivity);
		picasso.load("https://usercontent.googleapis.com/freebase/v1/image/" + celebrityModel.getId()).into(new Target() {
			@Override
			public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
				friendsImage.setImageBitmap(bitmap);
				PaletteAsyncListener listener = new PaletteAsyncListener() {
					@Override
					public void onGenerated(Palette palette) {
						Swatch swatch = palette.getVibrantSwatch();
						if (swatch != null) {
							ActionBar actionBar = baseActivity.getSupportActionBar();
							if (actionBar != null) {
								actionBar.setBackgroundDrawable(new ColorDrawable(swatch.getRgb()));
								actionBar.setStackedBackgroundDrawable(new ColorDrawable(swatch.getRgb()));
							}
						}
					}
				};
				Palette.generateAsync(bitmap, 24, listener);
			}

			@Override
			public void onBitmapFailed(Drawable errorDrawable) {

			}

			@Override
			public void onPrepareLoad(Drawable placeHolderDrawable) {

			}
		});
		nameText.setText(celebrityModel.getName());

		wikipediaDescription.setText(celebrityModel.getDescription());
		knownFor.setText(celebrityModel.getKnownFor());


	}


	@Override
	public void onResume() {
		super.onResume();
		baseActivity.getAnalyticsLogger().sendScreen("Celebrity Detail");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View friendDetailLayout = inflater.inflate(R.layout.celebretiesdetail, container, false);

		return friendDetailLayout;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		try {
			celebrityModel = getArguments().getParcelable(getActivity().getString(R.string.friends_extra));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (celebrityModel != null) {
			baseActivity.getSupportActionBar().setSubtitle(celebrityModel.getName());


			initiateView(view);
			populateFriendsDetail();

		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		baseActivity = (BaseActivity) getActivity();

	}


}
