/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.celites.wishnu.fragments;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.celites.wishnu.BaseActivity;
import com.celites.wishnu.utils.AnalyticsLogger;

/**
 * Simple Fragment used to display some meaningful content for each page in the sample's
 * {@link android.support.v4.view.ViewPager}.
 */
public class ContentFragment extends Fragment {

    private static final String KEY_TITLE = "title";
    private static final String KEY_INDICATOR_COLOR = "indicator_color";
    private static final String KEY_DIVIDER_COLOR = "divider_color";
	private int indicatorColor;
	private CharSequence sequence;

	/**
     * @return a new instance of {@link com.celites.wishnu.fragments.ContentFragment}, adding the parameters into a bundle and
     * setting them as arguments.
     */
    public static ContentFragment newInstance(CharSequence title, int indicatorColor,
            int dividerColor) {
        Bundle bundle = new Bundle();
        bundle.putCharSequence(KEY_TITLE, title);
        bundle.putInt(KEY_INDICATOR_COLOR, indicatorColor);
        bundle.putInt(KEY_DIVIDER_COLOR, dividerColor);

        ContentFragment fragment = new ContentFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
	    TextView view = new TextView(getActivity());
	    Bundle args = getArguments();
	    TextView title = (TextView) view;
	    sequence = args.getCharSequence(KEY_TITLE);
	    title.setText("Title: " + sequence);

	    indicatorColor = args.getInt(KEY_INDICATOR_COLOR);
	    title.setTextColor(indicatorColor);



        return view;
    }

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		AnalyticsLogger logger = ((BaseActivity)getActivity()).getAnalyticsLogger();
		logger.sendEvent("ActionbarColor","ColorLoad",sequence+" : "+indicatorColor);

		GradientDrawable drawable = new GradientDrawable();
		drawable.setColor(indicatorColor);
		((ActionBarActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(drawable);
	}
}
